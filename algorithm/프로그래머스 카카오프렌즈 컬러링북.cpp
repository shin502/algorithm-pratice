#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
bool visit[101][101];
vector<vector<int>> tt;
void dfs(int y , int x , vector<vector<int>>& picture , int val , int rw , int& cnt) {
    
    if (x >= picture[0].size() || y >= picture.size() || x < 0 || y < 0 || visit[y][x] == true || picture[y][x] != val || picture[y][x] == 0)
        return;
   
    tt[y][x] = rw;
    visit[y][x] = true;
    cnt++;

    dfs(y + 1, x, picture, val, rw, cnt);
    dfs(y - 1, x, picture, val, rw, cnt);
    dfs(y, x + 1, picture, val, rw, cnt);
    dfs(y, x - 1, picture, val, rw, cnt);
}

int main(void) {
    int m = 6;
    int n = 4;
    vector<vector<int>> picture = { {1, 1, 1, 0},{1, 2, 2, 0},{1, 0, 0, 1},{0, 0, 0, 1},{0, 0, 0, 3},{0, 0, 0, 3 }};
    /*int m = 1;
    int n = 1;
    vector<vector<int>> picture;
    picture.resize(1);
    picture[0].resize(1);
    picture[0][0] = 1;*/



    tt = picture;
    int rw = 1;

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            tt[i][j] = -1;
            visit[i][j] = false;

        }
    }

    int mc = 0;

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            if (visit[i][j] == true || picture[i][j] == 0)
                continue;
            int cnt = 0;
            dfs(i, j, picture, picture[i][j], rw++ , cnt);
            mc = max(mc, cnt);
        }
    }

   
    int number_of_area = rw - 1;
    int max_size_of_one_area = mc;

    vector<int> answer(2);
    answer[0] = number_of_area;
    answer[1] = max_size_of_one_area;
}