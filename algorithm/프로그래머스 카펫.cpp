#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
//카펫의 가로길이는 세로길이보다 길거나 같다

int main(void) {
	//brown과  yellow로 나올 수 있는 모든 경우의 수를 구하자
	int brown = 8;
	int yellow = 1;
	vector<int> answer;

	int tt = brown + yellow;
	
	vector<pair<int, int>> measure;
	for (int i = 1; i * i <= tt; ++i) {
		if (tt % i == 0) {
			measure.emplace_back(make_pair(i, tt / i));
		}
	}
	int cnt = 0;
	int width = 0;
	int length = 0;
	while (cnt != measure.size()) {
		 width = measure[cnt].second;
		 length = measure[cnt].first;

		 int tbrown = 0;
		 int tyellow = 0;
		 if (length == 1) {
			 tbrown = width;
			 tyellow = 0;
		 }
		 else {
			 tbrown = (width * 2) + ((length - 2) * 2);
			 tyellow = tt - tbrown;
		 }

		if (tbrown == brown && tyellow == yellow)
			break;
		++cnt;
	}

	answer.emplace_back(width);
	answer.emplace_back(length);
}