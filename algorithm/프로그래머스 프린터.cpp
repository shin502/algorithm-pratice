#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;

int main(void) {
	vector<int> priorities = { 1, 1, 9, 1, 1, 1 };
	int location = 0;

	vector<pair<int, int>> tp;
	for (int i = 0; i < priorities.size(); ++i) {
		tp.emplace_back(make_pair(priorities[i], i));
	}
	int answer = 0;
	while (!tp.empty()) {
		int val = tp.front().first;
		int idx = tp.front().second;
		auto mval = max_element(tp.begin() + 1, tp.end(), [](const pair<int,int>& a , const pair<int,int>& b) {
			return a.first < b.first;
		});
		if (val >= (*mval).first) {
			answer++;
			if (idx == location)
				break;
			tp.erase(tp.begin());

		}
		else {
			tp.emplace_back(make_pair(val, idx));
			tp.erase(tp.begin());
		}

	}

	return answer;

}