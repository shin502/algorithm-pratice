#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<queue>
using namespace std;




int main(void) {
	vector<int> scoville = { 1,2,3,9,10,12 };
	int K = 7;
	int answer = 0;
		
	priority_queue<int, vector<int>, greater<int>> pq;
	for (auto a : scoville) {
		pq.push(a);
	}

	while (!pq.empty()) {
		int m0 = pq.top();
		if (m0 >= K)
			break;
		else {
			pq.pop();
			if (pq.empty())
				break;
			int m1 = pq.top();
			pq.pop();
			int tmp = m0 + m1 * 2;
			pq.push(tmp);
			++answer;
		}

	}

	if (pq.empty())
		return -1;

	return answer;


}