#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

int main(void) {
	vector<int> citations = { 3, 0, 6, 1, 5 };
	int answer = 0;
	
	int mh = 0;
	auto me = max_element(citations.begin(), citations.end());
	for (int i = 0; i < *me; ++i) {
		int h = i;
		int cnt = 0;
		for (int j = 0; j < citations.size(); ++j) {
			if (citations[j] >= h) {
				++cnt;
			}
		}
		if (cnt >= h) {
			mh = max(mh, h);
		}
	}
	return mh;
}