#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<unordered_map>
#include<set>
using namespace std;

int main(void) {
	vector<string> phone_book = { "12", "123", "1235", "567", "88" };
	vector<string> phone_book = { "11","12","3" ,"4"};
	bool answer = true;
	
	unordered_map<string, int> um;
	set<int> slen;

	for (int i = 0; i < phone_book.size(); ++i) {
		int len = phone_book[i].length();
		slen.insert(len);
		um[phone_book[i]] += 1;
	}

	for (int i = 0; i < phone_book.size(); ++i) {
		for (auto& a : slen) {
			if (phone_book[i].size() > a) {
				string s = phone_book[i].substr(0, a);
				if(um[s] == 1)
					return false;
			}
		}
	}
	return true;

}