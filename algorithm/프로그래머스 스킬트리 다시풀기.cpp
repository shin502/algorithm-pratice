#include<iostream>
#include<vector>
#include<algorithm>
#include<string>

using namespace std;
int main(void) {
	string skill = "CBD";
	vector<string> skill_trees = { "BACDE", "CBADF", "AECB", "BDA" };
	int answer = 0;
	for (int i = 0; i < skill_trees.size(); ++i) {
		int idx = 0;
		bool flag = true;
		for (int j = 0; j < skill_trees[i].size(); ++j) {
			if (skill_trees[i][j] == skill[idx]) {
				++idx;
				continue;
			}
			for (int k = idx + 1; k < skill.size(); ++k) {
				if (skill_trees[i][j] == skill[k]) {
					flag = false;
					break;
				}
			}
			if (!flag)
				break;
		}
		if(flag)
		++answer;
	}

	cout << answer << endl;
}