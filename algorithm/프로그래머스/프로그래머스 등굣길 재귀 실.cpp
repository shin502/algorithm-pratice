#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
using namespace std;
int answer = 0;
int low = 1000000000;
void dfs(int cnt , int x , int y , vector<vector<int>>& puddles , int m , int n) {

	for (auto &a : puddles) {
		if (x == a[0] && y == a[1]) {
			return;
		}
	}

	if (x > m)
		return;
	if (y > n)
		return;

	if (x == m && y == n) {
		//최단경로가 새로 갱신된다면 answer를 다시 1로 초기화 
		if (cnt < low) {
			low = cnt;
			answer = 1;
		}
		else {
			answer++;
		}
	}

	dfs(cnt + 1, x + 1, y, puddles, m, n);
	dfs(cnt + 1, x, y + 1, puddles, m, n);
}

int main(void) {
	//집으로 갈 수 있는 경로를 알아야한다. 
	//오른쪽과 아래쪽으로만 움직여 갈 수 있는 최단경로 
	//학교는 맨 오른쪽끝에 있음 
	//1 ,1 에서 시작 
	int m = 4;
	int n = 3;
	vector<vector<int>> puddles = { {2,2} };
	
	dfs(0, 1, 1 , puddles , m , n);
	answer = answer % 1000000007;
	cout << answer << endl;

}