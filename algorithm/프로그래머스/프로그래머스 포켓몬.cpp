#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<set>
using namespace std;
//n / 2 마리의 포켓몬을 선택하는데 가장 많은 종류의 포켓몬을 선택해야함

int main(void) {
	vector<int> nums = {3,3,3,1,2,3};
	int n = nums.size() / 2;
	set<int> cg;

	for (int i = 0; i < nums.size(); ++i) {
		cg.insert(nums[i]);
	}
	int cnt = 0;
	for (auto& a : cg) {
		++cnt;
		if (cnt == n)
			return cnt;
	}

	return cnt;






}