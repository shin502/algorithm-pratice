#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main(void) {
	vector<string> phone_book = { "119", "97674223", "1195524421" };
	sort(phone_book.begin(), phone_book.end(), [](const string& a , const string& b) {
		return 	a.size() < b.size();
		});

	for (int i = 0; i < phone_book.size(); ++i) {
		for (int j = i + 1; j < phone_book.size(); ++j) {
			string p = phone_book[j].substr(0, phone_book[i].size());
			if (phone_book[i] == p) {
				return false;
			}
		}
	}
	return true;
	


}