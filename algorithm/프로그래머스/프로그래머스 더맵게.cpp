#include<iostream>
#include<vector>
#include<algorithm>
#include<queue>
using namespace std;

int main(void) {
	int K = 7;
	int answer = 0;
	vector<int> scoville = {8,10,11};
	priority_queue<int , vector<int> ,greater<int>> sc;
	for (int i = 0; i < scoville.size(); ++i) {
		sc.push(scoville[i]);
	}
	
	
	while (1) {
		int flag = true;
		int ms[2] = { K , K };

	

		int st = sc.top();
		if (st < K)
			flag = false;

		if (flag)
			break;

		if (sc.size() < 2) {
			return -1;
		}

		int sf = sc.top();
		sc.pop();
		int ss = sc.top();
		sc.pop();
		sc.push(sf + (ss * 2));

		answer++;
	}
	return answer;
}