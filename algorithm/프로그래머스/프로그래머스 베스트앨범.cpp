#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<map>
#include<set>
using namespace std;


//장르당 곡은 2곡을 선택
//장르당 곡이 하나라면 하나만 선택
//장르 내에서 많이 재생된 노래를 수록
//장르 내에서 재생횟수가 같다면 고유 번호가 낮은 노래를 수록 


int main(void) {
	vector<string> genres = { "classic","classic","classic","classic","pop" };
	vector<int> plays = {500 , 150 , 800, 800, 2500};
	map<string , int> m;
	map<string, multimap<int, int , greater<int>>> rm;
	vector<int> answer;

	//장르의 파악하고 정렬 및 저장
	//클래식 곡이 몇곡인지 알아야하고
	//팝곡이 몇곡인지 분류해서 저장할 자료구조 필요 
	for (int i = 0; i < genres.size(); ++i) {
		m[genres[i]] += plays[i];
		rm[genres[i]].insert(make_pair(plays[i] , i));
	}

	vector<pair<int, string>> vec;
	for (auto &a : m) {
		vec.emplace_back(a.second, a.first);

	}
	sort(vec.begin(), vec.end(), [](const pair<int,string>& a , const pair<int,string>& b) {
		return a.first > b.first;
		});


	//제일 많이 들은 순은 알아냈으니 2곡씩 꺼내야한다.
	for (auto &a : vec) {
		if (rm[a.second].size() == 1) {
			answer.emplace_back((*rm[a.second].begin()).second);
			continue;
		}
		else {
			int cnt = 0;
			for (auto &u : rm[a.second]) {
				answer.emplace_back(u.second);
				cnt++;
				if (cnt == 2)
					break;
			}
		}
		
	}

	for (auto & a : answer)
		cout << a << endl;
	



}