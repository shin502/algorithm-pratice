#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;

int main(void) {
	//vector<vector<int>> jobs = { {0, 9}, { 0, 4}, { 0, 5}, { 0, 7}, { 0, 3} };
	vector<vector<int>> jobs = {{24, 10}, {28, 39}, {43, 20}, {37, 5}, {47, 22}, {20, 47}, {15, 2}, {15, 34}, {35, 43}, {26, 1}};
	//vector<vector<int>> jobs = {{0, 5}, {1, 2}, {5, 5}};
	//vector<vector<int>> jobs = { {1, 9}, {1, 4}, {1, 5}, {1, 7}, {1, 3} };
	//vector<vector<int>> jobs = { {0, 1},{0, 1},{1, 0} };
	int time = 0;
	int vs = jobs.size();
	int total = 0;
	sort(jobs.begin(), jobs.end() ,[](const vector<int> &a ,const vector<int> &b) {
		return a[0] < b[0];
		});

	while (1) {
		int li = -1;
		int low = 1000000000;
		int tp = -1;
		//작업을 할수 있다면 골라야한다. 제일 빨리끝나는 작업을 고른다. 
		for (int i = 0; i < jobs.size(); ++i) {
			if (jobs[i][0] <= time) {
				int tt = jobs[i][1];
				if (tt < low) {
					low = tt;
					li = i;
					tp = (time + jobs[li][1]) - jobs[li][0];
				}
			}
		}
		//없다면 컨티뉴 
		if (li == -1) {
			time++;
//			time = jobs[0][0];
			continue;
		}
		//있다면 시간을 더해준 후에 배열에서 제거한다.

		time += jobs[li][1];
		total += tp;
		jobs.erase(jobs.begin() + li);

		if (jobs.size() == 0)
			break;


	}
	int answer = total / vs;
	cout << answer << endl;

}