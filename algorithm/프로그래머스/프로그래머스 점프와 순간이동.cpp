#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
//순간이동을 하면 X2배 한 수로 갈 수 있따.
//그러면 2로 나누면서 구해야겠네
//2로 안눠지면 -1을 한다 그러면 answer + 1


int main(void) {
	int n = 6;
	int answer = 0;
	while (n != 0) {
		if (n % 2 == 0)
			n = n / 2;
		else {
			n -= 1;
			answer++;
		}
	}
	return answer;
}