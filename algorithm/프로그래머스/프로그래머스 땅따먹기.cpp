#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
//시간복잡도가 너무 올라갈꺼같으면 저장을해라 -> 공간복잡도를 활용하여라 
//DP를 사용해라 
//피보나치 처럼 두번째 행 세번째 규칙으로 접근해보는것도 좋다.


int main(void) {
	vector<vector<int>> land = { {1, 2, 3, 5},{5, 6, 7, 100},{4, 3, 2, 200 }};
	//vector<vector<int>> land = {{1, 2, 3, 5}, {5, 6, 7, 8}, {4, 3, 2, 1}};
	if (land.size() == 1) {
		return *max_element(land[0].begin(), land[0].end());
	}


	for (int i = 1; i < land.size(); ++i) {
		for (int j = 0; j < land[i].size(); ++j) {
			int mv = -1;
			for (int k = 0; k < land[i - 1].size(); ++k) {
				if (k == j)
					continue;
				else
				{
					if (land[i - 1][k] > mv)
					{
						mv = land[i - 1][k];
					}
				}
			}
			land[i][j] += mv;
		}
	}
	vector<int> t = land[land.size() - 1];
	return *max_element(t.begin(), t.end());

}