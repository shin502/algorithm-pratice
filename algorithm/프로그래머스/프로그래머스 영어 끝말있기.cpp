#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
//끝말있기를 잘못하거나 단어를 중복되게 말하면 탈락 

int main(void) {
	int n = 2;
	//vector<string> words = { "tank", "kick", "know", "wheel", "land", "dream", "mother", "robot", "tank" };
	vector<string> words = { "hello", "one", "even", "never", "now", "world", "draw" };
	vector<int> answer = { 0 , 0 };
	
	vector<vector<string>> pw;
	pw.resize(n);
	for (int i = 0; i < words.size(); ++i) {
		//중복검사
		int idx = i % n;
		for (int j = 0; j < i; ++j) {
			if (words[i] == words[j]) {
				answer[0] = (idx + 1); // 몇번째 사람이  
				answer[1] = (i / n) + 1; // 몇번째 차례에서 
				return pw[idx].size() + 1;
			}

		}
		//끝말있기 잘못 했는지 검사 
		if (i != 0) {
			if (words[i - 1][words[i-1].size() - 1] != words[i][0]) {
				answer[0] = (idx + 1);
				answer[1] = (i / n) + 1;
				return idx + 1;
			}
		}
		pw[idx].emplace_back(words[i]);
	}
	return 0;
}