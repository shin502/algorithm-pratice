#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
struct file{
	string head;
	int number;
	int idx;
};
int main(void) {
	vector<string> files = {"img12.png", "img10.png", "img02.png", "img1.png", "IMG01.GIF", "img2.JPG"};
	vector<file> myFile;
	vector<string> answer;

	myFile.resize(files.size());


	
	for (int i = 0; i < files.size(); ++i) {
		string tfiles;
		//head
		int headidx = 0;
		for (int j = 0; j < files[i].size(); ++j) {
			if (files[i][j] <= '9' && files[i][j] >= '0') {
				headidx = j;
				break;
			}
		}
		myFile[i].head	= files[i].substr(0, headidx);
		transform(myFile[i].head.begin(), myFile[i].head.end(), myFile[i].head.begin(),::toupper);
		tfiles = files[i].substr(headidx);

		//number
		string temp;
		for (int j = 0; j < tfiles.size(); ++j) {
			if (tfiles[j] == '.')
				break;
			temp += tfiles[j];
		}
		myFile[i].number = stoi(temp);
		myFile[i].idx = i;
	}


	stable_sort(myFile.begin(), myFile.end(), [](const file& a, const file& b) {
		if (a.head < b.head)
			return true;
		else if (a.head == b.head) {
			return a.number < b.number;
		}

		return false;
	});

	for (auto& a : myFile) {
		answer.emplace_back(files[a.idx]);
	}

	cout << answer[0] << endl;





}