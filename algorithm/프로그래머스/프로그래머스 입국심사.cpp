#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;

int main(void) {
	//이분탐색이므로 반으로 나눠서 탐색한다.
	//최고 시간은 제일 오래걸린 입국심사대에서 모든 고객이 입국심사를 받는 경우이다.
	
	vector<int> times = {7 , 10};
	sort(times.begin(), times.end());
	long long answer;
	long long n = 6;
	long long min, max , avg;
	long long s = times.size();
	min = 1;
	max = times[s - 1] * n;
	
	//min값이 max보다 커진다면 탐색이 끝난다.-> 이분탐색 
	while (min <= max) {
		avg = (min + max) / 2;
		//현재 시간으로 돌릴수 있는 최대인원을 구하자 
		long long pp = 0;
		for (int i = 0; i < s; ++i) {
			pp += avg / times[i];
		}

		//pp가 구하고자 하는 값보다 크다면 
		if (pp >= n) {
			answer = avg;
			max = avg - 1;//pp가 크거나 같다면 더 작은 값이 나와야하므로 max를 avg - 1로 고친다. 
		}
		else {
			min = avg + 1;
		}
		//pp가 구하고자 하는 값보다 작다면 더 큰값이 나와야하므로 min값을 avg + 1로 고친다.

	}
	cout << answer << endl;
}