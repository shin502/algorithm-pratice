#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
struct tt {
	int h;
	int m;

	void plusClock(int t) {
		m += t;
		if (m >= 60) {
			int temp = m / 60;
			m = m % 60;
			h += temp;
		}
		else if (m < 0) {
			m = 60 + m;
			h = h - 1;
		}
		
	}
};
int main(void) {
	//int n = 2; // 셔틀 운행 횟수
	//int t = 1; // 셔틀 운행 간격
	//int m = 2; // 한 셔틀에 탈 수 있는 최대 크루 수 
	//vector<string> timetable = {"09:00", "09:00", "09:00", "09:00"}; // 크루가 대기열에 도착하는 시각을 모은 배열 

	//int n = 10; // 셔틀 운행 횟수
	//int t = 60; // 셔틀 운행 간격
	//int m = 45; // 한 셔틀에 탈 수 있는 최대 크루 수 
	//vector<string> timetable = {"23:59","23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", 
	//"23:59", "23:59"}; // 크루가 대기열에 도착하는 시각을 모은 배열 

	//int n = 2;
	//int t = 10;
	//int m = 2;
	//vector<string> timetable = { "09:10","09:09","08:00" };

	int n = 1;
	int t = 1;
	int m = 5;
	vector<string> timetable = { "00:01", "00:01", "00:01", "00:01", "00:01" };

	vector<tt> vtt;
	string answer;
	for (int i = 0; i < timetable.size(); ++i) {
		string hs;
		hs += timetable[i][0];
		hs += timetable[i][1];

		string hm;
		hm += timetable[i][3];
		hm += timetable[i][4];

		tt temp;

		temp.h = stoi(hs);
		temp.m = stoi(hm);
		
		vtt.emplace_back(temp);
	}
	//오름차순 정렬 
	sort(vtt.begin(), vtt.end(), [](const tt& a , const tt& b) {
		if (a.h < b.h) {
			return true;
		}
		else if (a.h == b.h) {
			return a.m < b.m;
		}

		return false;
	});


	//timetable을 정렬해야한다


	//콘이 제일 늦게 갈 수 있는 시간을 리턴해주면 된다.
	//1.최대 인원을 먼저 구함 -> timetable의 인원과 비교 timetable이 더 많다면 콘은 timetable사이의 시간에 들어가야함
	bool flag;
	int cnt = 0;
	tt tclock;
	tclock.h = 9;
	tclock.m = 0;
	//9시 시작으로 시간 간격마다 손님을 실음
	//최대시간을 구해야함 
	//못타는 경우 2가지를 구해서 최대시간을 구해야함
	//못타는 경우1 마지막시간에 사람이 많아서 못탐 -> 그러면 마지막에 탄사람보다 1분빨리와야함 
	// 마지막시간에 타야함 
	tt lastp = tclock;
	while (true) { // 몇시차 타는지 알아내자
		cnt++;

		flag = false;
		int mr = 0;
		for (int i = 0; i < vtt.size(); ++i) {
			if (vtt[i].h <= tclock.h && mr < m) {
				if (vtt[i].h == tclock.h && vtt[i].m > tclock.m) {
					continue;
				}
				lastp = vtt[i];
				vtt.erase(vtt.begin() + i);
				++mr;
				--i;
			}

			if (mr >= m)
				flag = true;
		}
		if (n <= cnt)
			break;
		tclock.plusClock(t); // 차 시간 
		
	}
	//이떄는 마지막에 탄사람
	//이떄는 막차시간
	//--> 이걸 구분짓는 요소가 무엇? 막차 시간에 사람이 max까지 탔냐 안 탔냐
	tt aclcok;
	if (flag == true) {
		lastp.plusClock(-1);
		aclcok = lastp;
	}
	else {//그냥 막차 시간에 타면 된다.
		aclcok = tclock;
	}
	
	string th = to_string(aclcok.h);
	string tm = to_string(aclcok.m);
	if (th.size() == 1) th = "0" + th;
	if (tm.size() == 1) tm = "0" + tm;
	answer = th + ":" + tm;


	return 1;

	


}