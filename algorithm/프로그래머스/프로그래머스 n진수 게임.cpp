#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main(void) {
	//10,2,16진법의 수가 나온다.
	int n = 16;
	int t = 16;//미리 구할 숫자의 개수 -> 튜브가 말해야할 숫자의 개수
	int m = 2; //참가 인원
	int p = 2; //튜브의 순서
	string numbers;
	int cnt = 0;
	while (true) {
		//n진법의 수를 구하자
		int temp = cnt;
		string s;
		while (true) {
			int na = temp % n;
			if (na >= 10)
				s += 'A' + (na - 10);
			else 
				s += to_string(na);
		
			temp = temp / n;
			if (temp == 0)
				break;
		}
		for (int i = s.size() - 1; i >= 0; --i) {
			numbers += s[i];
		}

		if (numbers.size() >= t * m)
			break;
		++cnt;
	}
	string answer = "";
	for (int i = 0; i <= t * m; ++i) {
		if (i % m == (p - 1)) {
			answer += numbers[i];
		}
		if (answer.size() == t)
			break;

	}
	cout << answer << endl;



}