#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;
int main(void) {
	int bridge_length = 2;
	int weight = 10;
	vector<int> truck_weights{ 7,4,5,6 };
	vector<pair<int ,int>> bridge_truck;
	int answer{ 0 };
	while (true) {
		//다리 위에 있는 트럭을 뺀다.
		for (int i = 0; i < bridge_truck.size(); ++i) {
			if (bridge_length == bridge_truck[i].first) {
				bridge_truck.erase(bridge_truck.begin() + i);
				--i;
			}
		}
		//다리 위에 있는 트럭의 무게를 구한다.
		int ttw = 0;
		for (int i = 0; i < bridge_truck.size(); ++i) {
			ttw += bridge_truck[i].second;
			//트럭의 경과시간을 올려준다.
			bridge_truck[i].first++;
		}
		
		//트럭을 다리 위로 넣는다.
		for (int i = 0; i < truck_weights.size(); ++i) {
			if (weight >= truck_weights[i] + ttw) {
				bridge_truck.emplace_back(make_pair(1, truck_weights[i]));
				ttw += truck_weights[i];
				truck_weights.erase(begin(truck_weights) + i);
				break;
				
			}
			else {
				break;
			}
		}
		++answer;

		//종료조건
		if (bridge_truck.size() == 0 && truck_weights.size() == 0) {
			break;
		}

	}
}