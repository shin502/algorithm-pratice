#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<map>
using namespace std;

//같은 종류이면 조합이 불가하다.
//1개씩 입을 수 있으므로 일단 1개 조합을 더해준다.
vector<vector<string>> clothes = { {"yellow_hat", "headgear"},{"blue_sunglasses", "eyewear"},{"green_turban", "headgear"} };
map<string, int> ct;

int main(void) {
	int answer = 0;
	for (int i = 0; i < clothes.size(); ++i) {
			ct[clothes[i][1]]++;
	}
	int val = 1;
	for (auto& a : ct) {
		val *= (a.second + 1);

	}

	answer = val - 1;

}

