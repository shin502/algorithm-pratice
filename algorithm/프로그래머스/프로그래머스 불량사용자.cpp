#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<set>
using namespace std;
//2 2 2 2 
//재귀로 풀어야할꺼같다.
//모든 경우의 수를 다 봐야함

/*
0 1 1 이나
1 1 0 이나 똑같이 취급해야한다.

*/
vector<bool> visit;
vector<vector<int>> numbers;

int tt = 0;
bool eqcheck() {
	for (int i = 0; i < numbers.size(); ++i) {
		int flag = true;
		for (int j = 0; j < numbers[i].size(); ++j) {
			if (visit[numbers[i][j]] == true)
				continue;
			else {
				flag = false;
				break;
			}
		}
		if (flag)
			return false;
	}

	return true;
}

void dfs(int n , vector<vector<int>>& ban) {
	if (n == ban.size())
	{
		if (eqcheck() == true) {
			numbers.emplace_back(vector<int>());
			for (int i = 0; i < visit.size(); ++i) {
				if (visit[i] == true) {
					numbers[tt].emplace_back(i);
				}
			}
			++tt;
		}
		return;
	}

	for (int i = 0; i < ban[n].size(); ++i) {
		int si = ban[n][i];
		if (visit[si] == false) {
			visit[si] = true;
			
			dfs(n + 1, ban);
			visit[si] = false;
		}
	}


}

int main(void) {
	//vector<string> user_id = { "frodo" , "fradi" , "crodo" , "abc123" , "frodoc" };
	//vector<string> banned_id = { "fr*d*", "*rodo", "******", "******" };
	vector<string> user_id = { "frodo", "fradi", "crodo", "abc123", "frodoc" };
	vector<string> banned_id = { "fr*d*", "abc1**" };

	visit.resize(user_id.size(), false);
	int answer{ 0 };
	vector<vector<int>> ban;
	ban.resize(banned_id.size());

	for (int i = 0; i < banned_id.size(); ++i) {
		for (int j = 0; j < user_id.size(); ++j) { // banned_id가 될 수 있는 user_id를 백터에다가 번호로 넣어준다 .
			if (banned_id[i].size() != user_id[j].size())
				continue;
			else {
				bool flag = true;
				for (int k = 0; k < user_id[i].size(); ++k) {
					if (user_id[j][k] == banned_id[i][k] || banned_id[i][k] == '*') continue;
					else {
						flag = false; break;
					}
				}
				if (flag)
					ban[i].emplace_back(j);//user_id를 넣는다.

			}

		}
	}

	dfs(0, ban);

	cout << tt << endl;
	return answer;


}