#include<iostream>
#include<vector>
#include<algorithm>
#include<string>

int gcd(int a, int b) {
	int r;
	while (b != 0) {
		int r = a % b;
		a = b;
		b = r;
	}
	return a;

}

using namespace std;
int main(void) {
	vector<int> arr = { 2,6,8,14 };
	sort(arr.begin(), arr.end());
	//최소 공배수 구하기
	//a * b / 최대 공약수 
	for (int i = 1; i < arr.size(); ++i) {
		int gcf	= (arr[i - 1] * arr[i]) / gcd(arr[i-1], arr[i]);
		arr[i] = gcf;
	}
	cout << arr[arr.size() - 1] << endl;



}