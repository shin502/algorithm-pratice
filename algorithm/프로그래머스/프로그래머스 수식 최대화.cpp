#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<cmath>
using namespace std;

int main(void) {
	string expression = "50*6-3*2";
	long long answer = 0;

	vector<int> op = { 0,1,2 }; 
	vector<char> oc{ '+', '-' , '*' };
	vector<string> tp;
	
	int pre = 0;
	//숫자와 연산자를 나눠서 계산
	for (int i = 0; i < expression.size(); ++i) {
		if (i == expression.size() - 1) {
			tp.emplace_back(expression.substr(pre, (i +1) - pre));
			break;
		}
		else if (expression[i] < 48) {//연산자라면 
			tp.emplace_back(expression.substr(pre,i - pre));
			
			tp.emplace_back(expression.substr(i,1));
			pre = i + 1;
		}
	}

	do {
		vector<string> tvec = tp;
		for (auto a : op) {
			long long value = 0;
			for (int i = 0; i < tvec.size(); ++i) {
				if (tvec[i].size() == 1 && tvec[i][0] == oc[a]) {
					int val = 0;
					if (oc[a] == '+')
						val = stoll(tvec[i - 1]) + stoll(tvec[i + 1]);
					else if (oc[a] == '-') {
						val = stoll(tvec[i - 1]) - stoll(tvec[i + 1]);
					}
					else if (oc[a] == '*') {
						val = stoll(tvec[i - 1]) * stoll(tvec[i + 1]);
					}
					for(int j=0; j < 2; ++j)
					tvec.erase(tvec.begin() + (i - 1));
					
					tvec[i - 1] = to_string(val);
					--i;
				}

			}
		}
		long long result = stoll(tvec[0]);
		answer = max(answer, abs(result));
	}
	while (next_permutation(op.begin(), op.end()));
	cout << answer << endl;


}