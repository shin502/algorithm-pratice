#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>

using namespace std;
int main(void) {
	vector<int> priorities{ 1,1,9,1,1,1 };
	int location = 0;
	int count = 0;
	vector<pair<int, int>> tp;

	for (int i = 0; i < priorities.size(); ++i) {
		tp.emplace_back(make_pair(i, priorities[i]));
	}

	while(1){

		pair<int,int> temp = tp.front();
		bool flag = true;
		for (auto &a : tp){
			if (a.second > temp.second) {
				tp.erase(tp.begin());
				tp.emplace_back(temp);
				flag = false;
				break;
			}
		}
		if (flag == true) {
			++count;
			if (location == temp.first) {
				cout << count;
				break;
			}
			tp.erase(tp.begin());
		}
	}
}