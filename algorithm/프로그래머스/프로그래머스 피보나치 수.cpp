#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int F[100000];

int main(void) {
	int n = 5;
	if (n == 1)
		return 1;
	if (n == 2)
		return 1;
	F[0] = 1;
	F[1] = 1;
	for (int i = 2; i < n; ++i) {
		F[i] = (F[i - 1] + F[i - 2]) % 1234567;
	}
	return F[n - 1];

}