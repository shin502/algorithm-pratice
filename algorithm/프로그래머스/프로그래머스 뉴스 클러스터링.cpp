#include<iostream>
#include<algorithm>
#include<vector>
#include<string>

using namespace std;
//들어온 str들을 변형한다. ->2개씩 묶음으로 특수문자는 버림
//교집합과 합집합을 구한다 -> 자카드 유사도를 구한다. * 65536을 곱한다.


int main(void) {

	string str1 = "E=M*C^2";
	string str2 = "e=m*c^2" ;

	vector<string> vec1;
	vector<string> vec2;

	


	for (auto &a : str1) {	a = toupper(a);}
	for (auto &a : str2) { a = toupper(a); }

	for (int i = 1; i < str1.size(); ++i) {
		if (isupper(str1[i]) == false || isupper(str1[i - 1]) == false)
			continue;
		string temp;
		temp += str1[i - 1];
		temp += str1[i];
		vec1.emplace_back(temp);
	}
	for (int i = 1; i < str2.size(); ++i) {
		if (isupper(str2[i]) == false || isupper(str2[i - 1]) == false)
			continue;
		string temp;
		temp += str2[i - 1];
		temp += str2[i];
		vec2.emplace_back(temp);
	}
	
	if (vec1.size() == 0 && vec2.size() == 0)
		return 65536;

	//자카드 검사
	//교집합 -> 있는것만 넣어
	vector<string> gyo;

	for (int i = 0; i < vec1.size(); ++i) {
		for (int j = 0; j < vec2.size(); ++j) {
			if (vec1[i] == vec2[j]) {
				string temp = vec1[i];
				gyo.emplace_back(temp);
				vec1.erase(vec1.begin() + i);
				vec2.erase(vec2.begin() + j);
				--i;
				break;
			}
		}
	}

	double hab = gyo.size() + vec1.size() + vec2.size();
	double tsize = gyo.size();
	//합집합 -> 교집합 둘다 빼고 나머지 다 넣어 
	double tt = 65536;

	int zacard = (tsize / hab) * tt;
	return zacard;


}