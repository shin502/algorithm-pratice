#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;
//이분탐색을 통하여 푼다.
//완전한 이분탐색은 아니고 중간값을 통하여 푸는것 
int main(void) {
	vector<int> stones = { 5,5,5,2,5,5,5,5,5,5 };
	int k = 3; // 연속 
	int first = 1;
	int second = *max_element(stones.begin(), stones.end());
	while (first <= second) {
		int mid = (first + second)/ 2;
		vector<int> ts = stones;
		int mc = 0;
		int count = 0;
		for (int i = 0; i < ts.size(); ++i) {
			ts[i] -= mid;
			if (ts[i] < 0)	ts[i] = 0;

			//연속 개수를 세자 
			if (ts[i] == 0) {
				count++;
				mc = max(mc, count);
			}
			else
				count = 0;
			
			if (mc >= k)
				break;
		}	
		
		if (mc >= k) // 연속된게 너무 많다 second를 줄인다 
			second = mid - 1;
		else if (mc < k) // 연속된게 너무 적다 
			first = mid + 1;
	}

	return first;

}