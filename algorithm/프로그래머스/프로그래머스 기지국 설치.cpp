#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;



int main(void) {
	int n = 16; // 아파트의 개수 
	vector<int> stations = { 1, 5 ,9,14 }; // //기지국이 설치된 아파트 번호 
	int w = 2; // 전파의 도달거리 
	int start = 0;
	int end = 0;
	int dm = w * 2 + 1;
	int answer = 0;

	int pet = 0;

	for (int i = 0; i < stations.size(); ++i) {
		int st = stations[i] - w - 1; // 시작 스테이션 
		int et = stations[i] + w - 1; // 끝나는 스테이션 
		if (et > n - 1)
			et = n - 1;
		if (st < 0)
			st = 0;
		if (i - 1 >= 0) {
			pet = stations[i - 1] + w - 1;
			if (pet >= st)
			{
				start = et + 1;
				continue;
			}
		}



		end = st;
		int cnt = end - start;
		int m = cnt / dm; // 몫
		if (cnt % dm != 0) { // 나머지가 있다면 
			m += 1;
		}
		answer += m;
		start = et + 1;
	}
	//마지막에 한번 더해줘야함
	
	if (n == start)
		return answer;
	else {
		int cnt = n - start;
		int m = cnt / dm; // 몫
		if (cnt % dm != 0) { // 나머지가 있다면 
			m += 1;
		}
		answer += m;

		return answer;
	}
}