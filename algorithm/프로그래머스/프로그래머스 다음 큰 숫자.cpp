#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
//n보다 커야함
//이진수 변환했을시 1의 갯수가 같음
//그 중 가장 작은 수

int main(void) {
	int n = 15;
	int tn = n;
	//1씩 올려서 찾기
	//이진수로 변환
	int one = 0;
	vector<int> t;
	while (n != 0) {
		if (1 == (n % 2)) ++one;
		n = n / 2;
	}
	int answer;
	while (true) {
		int to = 0;
		int tt;
		++tn;
		tt = tn;
		while (tt != 0) {
			if (1 == tt % 2) ++to;
			tt = tt / 2;
		}
		
		if (to == one) {
			answer = tn;
			break;
		}

	}
	return answer;



}