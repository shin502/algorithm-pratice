#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

//거쳐서 가는게 빠르면 최신화
//못가는길도 다 최신화
//K시간 이로 배달가능한 마을의 개수 return
//두 마을간에는 이동가능한 경로 존재 
constexpr int MAXROAD = 10001;
int main(void) {
	int N = 2;
	//vector<vector<int>> road = {{1, 2, 1}, {2, 3, 3}, {5, 2, 2}, {1, 4, 2}, {5, 3, 1}, {5, 4, 2}};
	vector<vector<int>> road = { {1, 2, 3}};
	int K = 2;

	int answer = 0;
	vector<vector<int>> town (N,vector<int>());
	for (int i = 0; i < town.size(); ++i) {
		town[i].resize(N, MAXROAD);
		town[i][i] = 0;
	}
	for (int i = 0; i < road.size(); ++i) {
		int f = road[i][0] - 1;
		int s = road[i][1] - 1;

		if (town[f][s] > road[i][2] || town[s][f] > road[i][2]) {
			town[f][s] = road[i][2];
			town[s][f] = road[i][2];
		}

	}
	
	//거쳐가는 노드
	for (int k = 0; k < town.size(); ++k) {
		//0
		//출발 노드
		for (int i = 0; i < town.size(); ++i) {
			//1
			//도착 노드 
			for (int j = 0; j < town.size(); ++j) {
				//4

				//1->2 = 1->0 0->2
				//1->3 = 
				if (i == j) continue;
				
				//출발->도착 > 출발->거쳐 + 거쳐+도착 비교 
				if (town[i][j] > town[i][k] + town[k][j]) {
					town[i][j] = town[i][k] + town[k][j];
				}

			}
		}
	}
	//플로이드 와샬 알고리즘


	
	for (int i = 0; i < town[0].size(); ++i) {
		if (town[0][i] <= K) {
			++answer;
		}
	}

	return answer;

}