#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
vector<vector<int>> tvec;
void hanoi(int n, int from, int tmp, int to) {	
	vector<int> tt;
	if (n == 1) {
		tt.emplace_back(from);
		tt.emplace_back(to);
		tvec.emplace_back(tt);// 기둥 1에서 1개의 원판을 기둥 3으로 옮긴다 .
	}
	else
	{
		hanoi(n - 1, from, to, tmp); // n-1개의 원판을 1에서  기둥 2로 옮긴다 
		tt.emplace_back(from);
		tt.emplace_back(to);
		tvec.emplace_back(tt);
		hanoi(n - 1, tmp, from, to); //n - 1 개의 원판을 2 에서 3으로 옮긴다. 
	}
}

int main(void) {
	int n = 2;
	hanoi(n, 1, 2, 3);
	return tvec[0][0];

}