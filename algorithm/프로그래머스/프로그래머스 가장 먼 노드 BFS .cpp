#include<iostream>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;
struct Node {
	vector<int> next;
	int dist = 0;
};
bool visited[6] = { false };
Node node[6];
int n = 6;
int line[7][2] = { {3,6}, {4,3} , {3,2} , {1,3} , {1,2} , {2,4} , {5 , 2} }; // 간선의 수 
int dt[6]; // 1번과의 거리를 담은 것
//간선 정보를통해 1번과의 거리를 계속 최신화 해준다


void bfs(int cnt, int curNode) {
	queue<int> q;
	q.push(curNode);
	while (1) {
		if (q.empty() == true)
			break;

		int start = q.front();
		q.pop();

		for (int i = 0; i < node[start].next.size(); ++i) {
			if (visited[node[start].next[i]] == false) {
				visited[node[start].next[i]] = true;
				q.push(node[start].next[i]);
				node[node[start].next[i]].dist = node[start].dist + 1;
			}
		}

	}

	
}


int main(void) {
	dt[0] = 0;
	visited[0] = true;
	for (int i = 1; i < n; ++i)
		dt[i] = 100000;

	for (int i = 0; i < 7; ++i) {
		node[line[i][0] - 1].next.emplace_back(line[i][1] - 1);
		node[line[i][1] - 1].next.emplace_back(line[i][0] - 1);
	} // 노드에다가 갈 수 있는 정보를 넣어준다.


	bfs(0 , 0);
	int mv = -1;
	for (int i = 0; i < 6; ++i) {
		mv = max(mv, node[i].dist);
	}
	int cnt = 0;
	for (int i = 0; i < 6; ++i) {
		if (mv == node[i].dist) {
			++cnt;
		}
	}

	cout << cnt << "\n";
}
