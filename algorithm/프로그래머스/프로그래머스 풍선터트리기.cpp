#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
using namespace std;
//DP로 푼다
//메모리에 최소값들을 저장하여 푼다
int main(void) {
	vector<int> a = { -16,27,65,-2,58,-92,-71,-68,-61,-33 };
	vector<int> left(a.size());
	vector<int> right(a.size());

	int MIN = a[0];
	for (int i = 0; i < a.size(); ++i) {
		MIN = min(MIN, a[i]);
		left[i] = MIN;
	}
	MIN = a[a.size() - 1];
	for (int i = a.size() - 1; i >= 0; --i) {
		MIN = min(MIN, a[i]);
		right[i] = MIN;
	}
	int answer = 0;
	for (int i = 0; i < a.size(); ++i) {
		if (left[i] >= a[i] || right[i] >= a[i]) ++answer;
	}
	return answer;



	



}