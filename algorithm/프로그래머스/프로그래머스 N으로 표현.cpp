#include<iostream>
#include<vector>
#include<algorithm>
#include<set>
#include<cmath>
using namespace std;
vector<set<int>> arr;

int main(void) {
	int N = 5;
	int number = 5;
	int answer;
	
	//2,22,222,2222 ... 을 vector[1] , [2] , [3] , [4] , [5] ..에 넣어준다 
	for (int i = 0; i < 8; ++i) {
		set<int> t;
		arr.emplace_back(t);
		if (i > 0)
			arr[i].insert(((N * (pow(10 , i)) + *arr[i - 1].begin())));
		else
			arr[i].insert(N);
	}
	if (N == number) {
		return 1;
	}
	//사칙연산을 통해 1번쓴거 2번쓴거 끼리 모아준다.
	for (int i = 1; i < 8; ++i) {
		for (int j = 0; j < i; ++j) {
			for (auto &x : arr[j]) {
				for (auto &y : arr[i - j - 1]) {
					arr[i].insert(x + y);
					arr[i].insert(x - y);
					arr[i].insert(x * y);
					if (y != 0) {
						arr[i].insert(x / y);
					}
				}
			}
		}
		if (arr[i].find(number) != arr[i].end()) {
			return i + 1;
		}

	}

	return -1;

	
}