#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int main(void) {
	vector<int> progresses{ 95, 90, 99, 99, 80, 99 };
	vector<int> speeds{ 1, 1, 1, 1, 1, 1 };

	vector<int> answer;
	while (true) {
		for (int i = 0; i < progresses.size(); ++i) {
			if (progresses[i] <= 100)
				progresses[i] += speeds[i];
		}
		int cnt = 0;
		while (progresses.size() > 0 && progresses[0] >= 100) {
			progresses.erase(progresses.begin());
			speeds.erase(speeds.begin());
			cnt++;
		}
		if (cnt > 0)
			answer.emplace_back(cnt);

		if (progresses.size() <= 0)
			break;

	}

}