#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
int val = 0;
vector<int> tnum;
void dfs(int n , int idx  ,int target) {
	
	if (n == target && idx == tnum.size()) {
		val++;
		return;
	}
	if (idx >= tnum.size())
		return;
	dfs(n + tnum[idx], idx + 1, target);
	dfs(n - tnum[idx], idx + 1, target);

}

int main(void) {
	vector<int> numbers{ 1,1,1,1,1 };
	int target = 3;

	tnum = numbers;
	dfs(0, 0, target);
	return val;
}