#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
vector<pair<int, int>> lt;

int main(void) {
	int answer = 0;

	vector<string> lines = {
			"2016-09-15 00:00:00.000 3s"
	};
	//끝나는시간 순서대로 정렬이 되어있다.
	//문자열에서 뽑아내어 ms로 바꿔보자
	
	for (int i = 0; i < lines.size(); ++i) {
		lines[i].pop_back();
		for (int j = 0; j < 11; ++j) {
			lines[i].erase(lines[i].begin());
		}
		int hour = stoi(lines[i].substr(0, 2)) * 3600000;
		int minute = stoi(lines[i].substr(3, 2)) * 60000;
		int second = stoi(lines[i].substr(6, 2)) * 1000;
		int milli = stoi(lines[i].substr(9, 3));

		int endTime = hour + minute + second + milli;

		int ip = stof(lines[i].substr(13, lines[i].size() - 1)) * 1000;

		int startTime = endTime - ip + 1;
		lt.emplace_back(make_pair(startTime, endTime));
		//종료시간 순서대로 정렬되어있다.
	}

	for (int i = 0; i < lt.size(); ++i) {
		int count = 0;
		for (int j = i; j < lt.size(); ++j) {
			if (lt[j].first < lt[i].second + 1000) {
				count++;
			}
		}

		if (count > answer)
			answer = count;
	}
	return answer;

}