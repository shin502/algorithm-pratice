//#include<iostream>
//#include<vector>
//#include<algorithm>
//#include<string>
//#include<map>
//using namespace std;
////기본점수 - 해당 웹페이지의 텍스트중 검색어 등장 횟수->대소문자무시
////외부 링크 수 -> 해당 웹페이지에서 다른 웹페잊로 연결된 링크의 개수
////링크점수-> 해당웹페이지로 링크가 걸린 다른 웹페이지의 => 기본점수 / 외부 링크 수 총합
////매칭 점수 -> 기본점수 + 링크점수 
//struct web {
//	int idx;
//	double basicpoint;
//	vector<string> links;
//	vector<int> linkidx;
//	double matchingpoint = 0;
//};
//
//
//int main(void) {
//	
//	vector<string> pages = {
//	"<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://a.com\"/>\n</head>  \n<body>\nBlind Lorem Blind ipsum dolor Blind test sit amet, consectetur adipiscing elit. \n<a href=\"https://b.com\"> Link to b </a>\n</body>\n</html>",
//
//"<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://b.com\"/>\n</head>  \n<body>\nSuspendisse potenti. Vivamus venenatis tellus non turpis bibendum, \n<a href=\"https://a.com\"> Link to a </a>\nblind sed congue urna varius. Suspendisse feugiat nisl ligula, quis malesuada felis hendrerit ut.\n<a href=\"https://c.com\"> Link to c </a>\n</body>\n</html>",
//
//"<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://c.com\"/>\n</head>  \n<body>\nUt condimentum urna at felis sodales rutrum. Sed dapibus cursus diam, non interdum nulla tempor nec. Phasellus rutrum enim at orci consectetu blind\n<a href=\"https://a.com\"> Link to a </a>\n</body>\n</html>"
//	};
//
//
//	/*vector<string> pages = { "<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://careers.kakao.com/interview/list\"/>\n</head>  \n<body>\n<a href=\"https://programmers.co.kr/learn/courses/4673\"></a>#!MuziMuzi!)jayg07con&&\n\n</body>\n</html>",
//
//	"<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://www.kakaocorp.com\"/>\n</head>  \n<body>\ncon%\tmuzI92apeach&2<a href=\"https://hashcode.co.kr/tos\"></a>\n\n\t^\n</body>\n</html>" };*/
//
//	//content=\"뒤에 자기 페이지 정보가 나온다.
//	//a href=\" 다음에는 외부 링크가 나온다.->어디로 쏘는지 저장해야함
//	//단어 찾아야함 => 기본점수 
//
//	
//	string word = "blind";
//	for (int k = 0; k < word.size(); ++k) {
//		word[k] = toupper(word[k]);
//	}
//	map<string , web> webs;
//
//	for (int i = 0; i < pages.size(); ++i) {
//		web tweb;
//		//인덱스 넣기
//		tweb.idx = i;
//		//내페이지 찾기
//		string temppage = pages[i];
//		string content = "<meta property=\"og:url\" content=\"https://";
//		int c = temppage.find(content);
//		int cnt = 0;
//		string mp = "";
//		while (temppage[c + cnt] != '>') {
//			mp += temppage[c + cnt];
//			cnt++;
//		}
//		int contentStart = content.size();
//		mp = mp.substr(contentStart);
//
//		mp.pop_back();
//		int wcnt = 0;
//
//		//단어 찾기 
//		for (int j = 0; j < pages[i].size(); ++j) {
//			string tword = pages[i].substr(j, word.size());
//			for (int k = 0; k < tword.size(); ++k) {
//				tword[k] = toupper(tword[k]);
//			}
//			if (word == tword && !isalpha(pages[i][j - 1]) && !isalpha(pages[i][j + (word.size())])) {
//				wcnt++;
//			}
//		}
//		tweb.basicpoint = wcnt;
//
//		string tpage = pages[i];
//		//링크찾기
//		while (true) {
//
//			string opstring = "a href=\"https://";
//			int pos = tpage.find(opstring);
//			if (pos == string::npos) {
//				break;
//			}
//			else {
//				
//				int tcnt = 0;
//				string op = "";
//				while (tpage[pos + tcnt] != '>'){
//					op += tpage[pos + tcnt];
//					tcnt++;
//				}
//				op = op.substr(opstring.size() - 1);
//
//				op.erase(op.begin());
//				tweb.links.emplace_back(op);
//				pos += 1;
//			}
//			tpage = tpage.substr(pos);
//
//		}
//		webs[mp] = tweb;
//	}
//	double answer = 0;
//	int idx = 0;
//	for (auto& a : webs) {
//		web& aweb = a.second;
//		for (auto& b : webs) {
//			if (a.first != b.first) {
//				web bweb = b.second;
//				bool flag = false;
//				for (int i = 0; i < bweb.links.size(); ++i) {
//					if (a.first == bweb.links[i]) {
//						flag = true;
//						break;
//					}
//				}
//				if (flag) {
//					double linkpoint = bweb.links.size();
//
//					aweb.matchingpoint += (bweb.basicpoint / linkpoint);
//				}
//
//			}
//		}
//		aweb.matchingpoint += aweb.basicpoint;
//	}
//	for (auto& a : webs) {
//		if (answer < a.second.matchingpoint) {
//			answer = a.second.matchingpoint;
//			idx = a.second.idx;
//		}
//	}
//
//	return idx;
//	
//}


#include <string>
#include <vector>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

bool cmp(pair<double, double> p1, pair<double, double> p2) {
    if (p1.first == p2.first)
        return p1.second < p2.second;
    else
        return p1.first > p2.first;
}

string make_lowercase(string s) {
    for (int i = 0; i < s.length(); i++) {
        if (s.at(i) >= 'A' && s.at(i) <= 'Z') {
            s.at(i) += 32;
        }
    }
    return s;
}

int solution(string word, vector<string> pages) {
    int answer = 0;
    double score[20] = { 0 };
    double result[20] = { 0 };
    map<string, int> url_map;
    set<string> url_v[20];

    word = make_lowercase(word);
    vector<pair<double, double>> v;

    for (int i = 0; i < pages.size(); i++)
    {
        string str = pages.at(i);
        string meta = "<meta";
        string https = "\"https://";
        string https_end = "\"";
        string a = "<a href=\"";
        string a_end = "\"";

        // Find URL
        size_t found = 0;
        while (true) {
            found = str.find(meta, found);
            if (found == string::npos)
                break;

            string temp = str.substr(found + meta.length());
            temp = temp.substr(0, temp.find(">"));

            found += meta.length();
            if (temp.find(https) != string::npos) {
                size_t found = temp.find(https_end, temp.find(https) + https.length());
                temp = temp.substr(temp.find(https) + 1, found - 1 - temp.find(https));
                url_map[temp] = i;
            }
        }

        // Find a href
        found = 0;
        while (true) {
            found = str.find(a, found);
            if (found == string::npos)
                break;

            string temp = str.substr(found + a.length());
            temp = temp.substr(0, temp.find("\">"));

            found += a.length();
            url_v[i].insert(temp);
        }

        // Find a word
        str = make_lowercase(str);
        found = str.find(word, 0);
        while (found != string::npos) {
            score[i] += 1;
            if ((found >= 1 && str.at(found - 1) >= 'a' && str.at(found - 1) <= 'z') ||
                (found + word.length() < str.length() - 1 && str.at(found + word.length()) >= 'a' && str.at(found + word.length()) <= 'z')) {
                score[i] -= 1;
            }

            found = str.find(word, found + word.length());
        }
    }

    // a href scoring
    for (int i = 0; i < pages.size(); i++) {
        if (url_v[i].size() == 0)
            continue;

        double temp = score[i] / url_v[i].size();
        for (set<string>::iterator it = url_v[i].begin(); it != url_v[i].end(); it++) {
            if (url_map.count((*it)) == 0)
                continue;
            result[url_map[(*it)]] += temp;
        }
    }

    // Sorting
    for (int i = 0; i < pages.size(); i++) {
        cout << score[i] + result[i] << endl;
        v.push_back(make_pair(result[i] + score[i], i));
    }

    sort(v.begin(), v.end(), cmp);
    answer = v.at(0).second;
    cout << answer << endl;

    return answer;
}