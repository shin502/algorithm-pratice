#include<iostream>
#include<string>
#include<algorithm>
#include<set>
#include<map>
#include<vector>
using namespace std;
map<tuple<int, int, int>, int> m;
bool check() {
	for (auto &t : m) {
		int x, y, a;
		tie(x, y, a) = t.first; 
		
		if (a == 0) // 기둥이라면 
		{
			if (y == 0) //y가 0이라면 
				continue;
			if(m.count(make_tuple(x,y - 1, 0))) // 내 아래 바로 기둥이 있다면 
				continue;
			if (m.count(make_tuple(x, y, 1)) || m.count(make_tuple(x - 1, y, 1))) //내 같은 위치에 보가 있거나 왼쪽에 보가 있거나
				continue;

			return false;
		}
		else if (a == 1) // 보라면 
		{
			if (m.count(make_tuple(x, y - 1, 0))) // 내 아래 바로 기둥이 있다면 
				continue;
			if (m.count(make_tuple(x - 1, y, 1)) && m.count(make_tuple(x + 1, y, 1))) //내 옆에 보가 있고 내 오른쪽에도 보가 있다면 
				continue;
			if (m.count(make_tuple(x + 1, y - 1, 0)))//내 오른쪽에 기둥이 있다면 
				continue;

			return false;
		}

	}
	return true;

}
int main(void) {
	int n;
	vector<vector<int>> build_frame = { {0, 0, 0, 1}, {2, 0, 0, 1}, {4, 0, 0, 1}, {0, 1, 1, 1}, {1, 1, 1, 1}, {2, 1, 1, 1}, {3, 1, 1, 1}, {2, 0, 0, 0}, {1, 1, 1, 0}, {2, 2, 0, 1} };

	for (auto b : build_frame) {
		tuple<int, int, int> tb = make_tuple(b[0], b[1], b[2]); // x , y , a -> 0이면 기둥 , 1이면 보 
		int t = b[3]; // 설치 삭제 0이면 삭제 1이면 설치 

		if (t == 0) { //삭제 
			m.erase(tb); // 일단 삭제 
			if (!check()) {
				m[tb] = 1; //  다시 넣기 
			}
		}
		else if (t == 1) // 추가 
		{
			//일단 추가
			m[tb] = 1;
			if (!check()) {
				m.erase(tb);
			}
		}
	}
	vector<vector<int>> answer;

	for (auto &b : m) {
		vector<int> t;
		int x, y, a;
		tie(x, y, a) = b.first;
		t = { x , y, a };
		answer.emplace_back(t);
	}

	sort(answer.begin(), answer.end(), [](auto& a , auto& b) {
		if (a[0] == b[0]) {
			if (a[1] == b[1]) {
				return	a[2] < b[2];
			}
			return a[1] < b[1];
		}
		return a[0] < b[0];
	});
	
	cout << answer[0][0] << endl;

}