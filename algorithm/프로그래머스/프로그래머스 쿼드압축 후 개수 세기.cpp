#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
//재귀적으로 풀어야함 4개로 나누기 -> 나눈거에서 또 4개로 나눔. -> 나눴는데 모두 1X1이 된다면 그만 나눔 


using namespace std;
int numZero = 0;
int numOne = 0;
void check(bool& isSame, int& prevalue, int val , bool flag) {
	if (isSame == true && prevalue != -1 && prevalue != val) {
		isSame = false;
	}
	else {
		prevalue = val;
	}
}
void divide(vector<vector<int>> darr) {

	//4개로 나눈다.
	vector<vector<int>> quad[4];
	bool flag = true;
	bool isSame[4] = {true , true , true , true};
	int prevalue[4] = { -1 , -1, -1 , -1};
	for (int i = 0; i < 4; ++i) {
		quad[i].resize(darr.size() / 2);
	}
	int len = darr.size();
	for (int i = 0; i < darr.size(); ++i) {
		for (int j = 0; j < darr[i].size(); ++j) {
			if (i < (len / 2) && j < (len / 2)) { // i = 0 1 , j = 0 1 
				quad[0][i].emplace_back(darr[i][j]);
				check(isSame[0], prevalue[0], darr[i][j], flag);
			}
			else if (i < (len / 2) && j >= (len / 2)) { // i = 0 1 , j = 2 3 
				quad[1][i].emplace_back(darr[i][j]);
				check(isSame[1], prevalue[1], darr[i][j],flag);
			}
			else if (i >= (len / 2) && j < (len / 2)) { // i = 2 3 , j = 0 1 
				quad[2][i % (len / 2)].emplace_back(darr[i][j]);
				check(isSame[2], prevalue[2], darr[i][j],flag);
			}
			else if (i >= (len / 2) && j >= (len / 2)) { // i = 2 3 , j = 2 3
				quad[3][i % (len / 2)].emplace_back(darr[i][j]);
				check(isSame[3], prevalue[3], darr[i][j],flag);
			}
		}
	}

	for (int i = 0; i < 4; ++i) {
		if (isSame[i] == false) {
			flag = false;
			break;
		}
		for (int j = 0; j < 4; ++j) {
			if (prevalue[i] != prevalue[j]) {
				flag = false;
				break;
			}
		}
	}

	//모두 4면을 검사하고 면이 모두 1x1인지 검사후 맞다면 더해줌
	if(flag == true){
		prevalue[0] == 0 ? numZero++ : numOne++;
	}
	else {
		for (int i = 0; i < 4; ++i) {
			if (isSame[i] == true) {
				if (prevalue[i] == 0)
					numZero++;
				else
					numOne++;
			}
			else {
				divide(quad[i]);
			}
		}
	}
	//아니라면 1X1이 아닌 면만 재귀를 돌린다.
}

int main(void) {
	//vector<vector<int>> arr = { {0, 0, 0, 0} ,  {0, 0, 0, 0}  ,  {0, 0, 0, 0}  ,  {0, 0, 0, 0} };
	vector<vector<int>> arr = { {1, 1, 1, 1, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 1, 1, 1}, {0, 0, 0, 0, 1, 1, 1, 1}, {0, 1, 0, 0, 1, 1, 1, 1}, {0, 0, 0, 0, 0, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 1, 0, 0, 1}, {0, 0, 0, 0, 1, 1, 1, 1} };
	divide(arr);

	vector<int> answer = {numZero , numOne};

}