#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
using namespace std;
bool board[12][12] = { false, };
//01 12 23

//1 3 5 7 
//00 11 22 33 44
//2 4 6 8
//02 13 2 4 
//2 4 
//- + 
//03 11 0 2
//30 21 12 03
int tt = 0;
bool check(int hang , int yall) {
	for (int i = 0; i < 12; ++i) {
		if (yall != i && board[hang][i] == true) //세로 
			return false;
		if (hang != i && board[i][yall] == true) { // 가로  
			return false;
		}
	}

	for (int i = 0; i < 12; ++i) {
		for (int j = 0; j < 12; ++j) {
			if (hang + yall == i + j && hang != i && yall != j && board[i][j] == true) {
				return false;
			}

			if (yall - hang == j - i && hang != i && yall != j && board[i][j] == true) {
				return false;
			}
		}
	}
	return true;
}
void dfs(int n , int cnt) {

	if (n == cnt) {
		++tt;
		return;
	}


	for (int i = 0; i < cnt; ++i) {
		if (board[n][i] == false) {
			board[n][i] = true;
			if (check(n, i) == true)
				dfs(n + 1, cnt);
			board[n][i] = false;
		}
	}

}
int main(void) {
	//가로 세로길이 
	int n = 12;
	dfs(0, n);
	return tt;
}