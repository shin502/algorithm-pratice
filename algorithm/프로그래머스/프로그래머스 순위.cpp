#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;
//플로이드 와샬 알고리즘을 사용하여 해결한다.
//등수를 모르는 것은 -1로 표기
//폴리읃 와샬 알고리즘을 통하여 등수를 갱신한다.

int main(void) {
	int n = 8;
	vector<vector<int>> results = {{1, 2}, {2, 3}, {3, 4}, {5, 6}, {6, 7}, {7, 8}};
	vector<vector<int>> arr;
	int answer = 0;
	arr.resize(n);
	for (int i = 0; i < arr.size(); ++i) {
		arr[i].resize(arr.size());
		for (int j = 0; j < arr[i].size(); ++j) {
			arr[i][j] = 0;
		}
	}
	
	for (int i = 0; i < results.size(); ++i) {
		//등수를 알아내기 위해 표를 보고 arr에다가 승패를 갱신해준다.
		int w = results[i][0] - 1;
		int l = results[i][1] - 1;

		arr[w][l] = 1;
		arr[l][w] = -1;
	}

	//내가 누군가한테 진다면 누군가가 이긴놈들한테도 지는것
	//내가 누군가를 이긴다면 누군가는 내가 이긴놈들한테 다 지는것

	//와샬 알고리즘을 통하여 표를 다 채운다. 1이 2를 이김 -> 3은 2한테 짐 -> 1은 3을 이김 이런식으로 채운다
	for (int k = 0; k < arr.size(); ++k) { // 나
		for (int i = 0; i < arr.size(); ++i) { // 누군가 
			for (int j = 0; j < arr.size(); ++j) { // 제 3삼자들

				if (arr[k][i] == 1) { //내가 누군가를 이긴다면 내가 졌던 사람들한테 애도 짐  
					if (arr[k][j] == -1) {
						arr[i][j] = -1;
					}
				}
				else if (arr[k][i] == -1) { // 내가 누군가한테 졌다면 누군가는 내가 이겼던 사람도 이김 
					if (arr[k][j] == 1) {
						arr[i][j] = 1;
					}
				}
			}
		}
	}


	for (int i = 0; i < arr.size(); ++i) {
		bool flag = true;
		for (int j = 0; j < arr.size(); ++j) {
			if (i != j && arr[i][j] == 0) {
				flag = false;
				break;
			}
		}

		if (flag)
			answer++;
	}

	cout << answer <<endl;
} 