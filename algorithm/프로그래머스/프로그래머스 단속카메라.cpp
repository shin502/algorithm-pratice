#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main(void) {
	vector<vector<int>> routes = { {-2, -1}, {1, 2}, {-3, 0} };
	sort(routes.begin(), routes.end(),[](const vector<int>& a , const vector<int>& b) {
		return a[1] < b[1];
	});
	//진출 시점으로 정렬 
	//다음거의 진입 시점이 나의 진출 시점보다 작거나 같을떄 + 1 아니면 Keep going

	int outdoor = -10000000;
	int cam = 0;
	for (int i = 0; i < routes.size(); ++i) {
		if (routes[i][0] > outdoor) {
			cam += 1;
			outdoor = routes[i][1];
		}
	}
	
	return cam;


}