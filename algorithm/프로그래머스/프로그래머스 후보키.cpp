#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
//조합을 봐야한다.
//비트연산으로 풀어야한다.
//부분집합의 개수는 2의 n승이다
int countBits(int n) {
	int cnt = 0;
	while (n != 0) {
		if (n & 1) ++cnt;
		n = n >> 1; // n을 1만큼 오른쪽으로 쉬프트
		
	}
	return cnt;
}
bool comp(int& a, int& b) {
	return countBits(a) < countBits(b);
}

bool check(int row, int col, vector<vector<string>>& relation, int subset) { //subset을 넣어준것은 and 연산을 해서 그부분의 열들이 유일성이 있는가 테스트
	for (int i = 0; i < row - 1; ++i) {
		for (int j = i + 1; j < row; ++j) {
			bool flag = true;
			for (int k = 0; k < col; ++k) {
				if (((subset & (1 << k)) == 0)) continue; // 1 << k를 하면 0001 , 0010 0100 1000 나오므로 어떤 열이 유일성을 가지는지 관해 테스트가능
				if (relation[i][k] != relation[j][k]) { // 하나라도 아래있는 것들과 같지 않다면 break
					flag = false;
					break;
				}

			}
			if (flag == true) // 만약 아래있는 것들과 모두 같다면 return false;
				return false;
		}
	}

	return true;

}

int main(void) {
	vector<vector<string>> relation = {{"100", "ryan", "music",  "2"},
									  {"200", "apeach", "math",  "2"},
									  {"300", "tube", "computer","3"},
									  {"400", "con", "computer", "4"},
									  {"500", "muzi",  "music",  "3"},
									  {"600", "apeach","music",  "2"}};
	
	vector<int> candidate;

	int row = relation.size();//행의 개수
	int col = relation.front().size(); //열의 개수

	//유일성 확인 
	for (int i = 1; i < (1 << col); ++i) {
		if (check(row , col , relation , i) == true) {
			candidate.emplace_back(i);
		}
	}


	//최소성확인
	sort(candidate.begin(), candidate.end(), comp);

	for (int i = 0; i < candidate.size(); ++i) {
		for (int j = i + 1; j < candidate.size(); ++j) {
			if ((candidate[i] & candidate[j]) == candidate[i]) {
				candidate.erase(candidate.begin() + j);
				j = i;
			}
		}
	}
	return candidate.size();
	//비트연산할때 괄호를 꼭 넣어주어야한다.
}