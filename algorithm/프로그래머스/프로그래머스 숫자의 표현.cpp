#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

//1부터 n까지 터하는 점화식을 이끌어내어야함
int main(void) {
	int n = 4;
	int answer = 1;


	for (int i = 1; i < n; ++i) {
		int j = i + 1;
		while (true) {
			int tt;
			if ((j - i) % 2 == 0) {
				tt =(i + j) * ((j - i) / 2) + ((i + j) / 2);
			}
			else {
				tt = (i + j) * ((j - i + 1) / 2);
			}

			if (tt > n)
				break;
			if (tt == n) {
				++answer;
				break;
			}
			++j;
		}
	}
	cout << answer << endl;


}