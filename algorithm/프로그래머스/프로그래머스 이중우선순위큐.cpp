#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<sstream>
using namespace std;
int main(void) {

//	vector<string> operations = { "I 7", "I 5", "I -5", "D -1" };
	vector<string> operations = { "I 16", "I -5643", "D -1", "D 1", "D 1", "I 123", "D -1" };
	vector<int> q;
	vector<int> answer;


	for (int i = 0; i < operations.size(); ++i) {
		if (operations[i][0] == 'D' && q.size() > 0)
		{
			if (operations[i][2] == '-') {
				q.erase(q.begin());
			}
			else {
				q.erase(q.end() - 1);
			}
		}
		else if(operations[i][0] == 'I')
		{
			int t;
			operations[i] = operations[i].substr(2);
			stringstream ssInt(operations[i]);
			ssInt >> t;
			q.emplace_back(t);
			sort(q.begin(), q.end());
		}
	}

	if (!q.empty()) {
		answer.emplace_back(*(q.end() - 1));
		answer.emplace_back(*q.begin());
	}
	else {
		answer.emplace_back(0);
		answer.emplace_back(0);
	}

}