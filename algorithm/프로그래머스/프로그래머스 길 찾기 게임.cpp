#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
struct Node {
	int idx;
	int x;
	int y;
	vector<Node> next; //0번 왼쪽 1번 오른쪽 
	Node() {
		idx = -1;
		x = -1;
		y = -1;
	}
};
Node root;

vector<int> pf;//후위
vector<int> pn;//전위

void preorder(Node* pointer) {

	if (!pointer->next.empty()) {
		preorder(&pointer->next[0]);//left
		preorder(&pointer->next[1]);
	}
		if (pointer->idx != -1)
		pf.emplace_back(pointer->idx);
	
}

void postorder(Node* pointer) { // 전위

	
		if(pointer->idx != -1)
		pn.emplace_back(pointer->idx);

	if (!pointer->next.empty()) {
		postorder(&pointer->next[0]);//left
		postorder(&pointer->next[1]);	
	}
}


int main(void) {
	//이진트리를 구성한다 ->  x값으로
	//y값은 항상 부모 노드 보다 작다
	vector<vector<int>> nodeinfo = { {5,3},{11,5},{13,3},{3,5},{6,1},{1,3},{8,6},{7,2},{2,2} };
	//y값으로 정렬 

	vector<Node> tempnode;
	tempnode.resize(nodeinfo.size());
	for (int i = 0; i < tempnode.size(); ++i) {
		tempnode[i].x = nodeinfo[i][0];
		tempnode[i].y = nodeinfo[i][1];
		tempnode[i].idx = i + 1;

	}



	sort(tempnode.begin(), tempnode.end(), [](Node& a , Node& b) {
		return a.y < b.y;
	});


	root = tempnode.back();
	tempnode.pop_back();

	while (!tempnode.empty()) {
		Node node = tempnode.back();
		tempnode.pop_back();

		Node* temp = &root;
		while (true) {
			if (temp->x > node.x) { //왼쪽
				if (temp->next.empty()) {
					temp->next.resize(2);
				}

				if (temp->next[0].x == -1) {
					temp->next[0] = node;
					break;
				}
				else {
					temp = &temp->next[0];
				}
			}
			else if (temp->x < node.x) { // 오른쪽 

				if (temp->next.empty()) {
					temp->next.resize(2);
				}

				if (temp->next[1].x == -1) {
					temp->next[1] = node;
					break;
				}
				else {
					temp = &temp->next[1];
				}
			}
										
		}
	}
	Node* temp = &root;
	preorder(temp);
	temp = &root;
	postorder(temp);
	vector<vector<int>> answer;
	
	answer.emplace_back(pn);
	answer.emplace_back(pf);
	

}