#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<set>
using namespace std;

//모든 사원이 무작위로 자연수 하나씩
//각 사원은 딱 한번 경기
//A : 1 B : 1 서로의 수 공개 큰쪽이 승리 , 승리한 사원이 속한 팀은 승점 1 점 
//숫자 같으면 둘다 0 
//최대 얻을 수 있는 점수를 구하여라...
//격차 비교를 하지 않고 변수  두개를 써서 B가 이겼을때는 Ap와 Bp를 증가시고 A가 이겼을때는 Ap만 증가시키면 해결 가능

int main(void) {
	//  3 1
	//  2 2 

	vector<int> A = {5,1,3,7};
	vector<int> B = {2,2,6,8};
	sort(A.begin(), A.end() , greater<int>());
	sort(B.begin(), B.end() , greater<int>());
	int Ap = 0;
	int Bp = 0;
	int answer = 0;
	for (int i = 0; i < A.size(); ++i) {
		if (A[Ap] < B[Bp]) {
			Bp++;
			Ap++;
			answer++;
		}
		else {
			Ap++;
		}

	}
	return answer;
}

