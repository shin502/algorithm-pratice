#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
//지나쳐온 길들을 표시해야한다.
bool arr[11][11][4]{ false }; // 오른쪽 아래 왼쪽 위 
int gox[4] = { 1, 0, -1, 0 };
int goy[4] = { 0, -1, 0 ,1 };

bool rcheck(int x, int y) {
	if (x < 0 || y < 0 || y > 10 || x > 10)
		return false;

	return true;
}
int main(void) {

	string dirs = "RRRRRRRRRRRRR";
	int answer = 0;
	int sx = 5;
	int sy = 5;
	
	//내가 지나온 길인지 확인 
	for (int i = 0; i < dirs.size(); ++i) {
		int dir = -1;
		switch (dirs[i]) {
		case 'R':
			dir = 0;
			break;
		case 'D':
			dir = 1;
			break;
		case 'L':
			dir = 2;
			break;
		case 'U':
			dir = 3;
			break;
		}

		int tx = sx;
		int ty = sy;

		tx += gox[dir];
		ty += goy[dir];

		if (!rcheck(tx, ty))
			continue;


		if (arr[sx][sy][dir] == false) {
			answer++;
			arr[sx][sy][dir] = true;
			int tdir = (dir + 2) % 4;
			sx = tx;
			sy = ty;
			arr[sx][sy][tdir] = true;
		}
		else
		{
			sx = tx;
			sy = ty;
		}
	}
	
	return answer;


}