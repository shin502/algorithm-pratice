#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;



int main(void) {
	string s = "1 2 3 4";
	string answer;
	vector<int> vec;	
	for (int i = 0; i < s.size(); ++i) {
		string ts = "";
		if (s[i] == ' ')
			continue;
		else {
			while (s[i] != ' ') {
				if (i == s.size()) break;
				ts += s[i];
				++i;
			}
			vec.emplace_back(stoi(ts));
		}
	}
	sort(vec.begin(), vec.end());
	answer += std::to_string(vec[0]);
	answer += " ";
	answer += std::to_string(vec[vec.size() - 1]);
}