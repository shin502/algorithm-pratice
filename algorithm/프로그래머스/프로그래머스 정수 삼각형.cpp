#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
	vector<vector<int>> triangle = { { 7 }, {3, 8 }, {8, 1, 0} , {2, 7, 4, 4}, {4, 5, 2, 6, 5} };
	int answer;
	for (int i = 1; i < triangle.size(); ++i) {
		for (int j = 0; j < triangle[i].size(); ++j) {
			if (j == 0) {
				triangle[i][j] += triangle[i - 1][j];
			}
			else if (j == triangle[i].size() - 1) {
				triangle[i][j] += triangle[i - 1][j - 1];
			}
			else {
				triangle[i][j] += max(triangle[i - 1][j - 1], triangle[i - 1][j]);
			}
			if (i == triangle.size() - 1)
				answer = max(answer, triangle[i][j]);
		}
	}
	return answer;

}