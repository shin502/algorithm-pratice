#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
//최대 100개이다.
int arr[101][101] = { 0, };
int main(void) {
	//집으로 갈 수 있는 경로를 알아야한다. 
	//오른쪽과 아래쪽으로만 움직여 갈 수 있는 최단경로 
	//학교는 맨 오른쪽끝에 있음 
	//1 ,1 에서 시작 
	int m = 4;
	int n = 3;
	vector<vector<int>> puddles = { {2,2} };
	int answer = 0;
	for (auto& a : puddles)
	{
		arr[a[1] - 1][a[0] - 1] = -1;
	}
	arr[0][0] = 1;

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			if (arr[i][j] == -1) {
				arr[i][j] = 0;
				continue;
			}


			if (i == 0) {
				arr[i][j] += arr[i][j - 1];
			}
			else if (j == 0) {
				arr[i][j] += arr[i - 1][j];
			}
			else {
				arr[i][j] += (arr[i][j - 1] + arr[i - 1][j]) % 1000000007;
				
			}
		}
	}


	answer = arr[n - 1][m - 1];
	cout << answer << endl;
}