#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int LUT[] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,12,11,10,9,8,7,6,5,4,3,2,1 };

int main() {
	string name = "AAJJAA";
	int answer = 0;
	for (auto ch : name)
		answer += LUT[ch - 'A'];
	int len = name.length();
	int left_right = len - 1;
	for (int i = 0; i < len; ++i) {
		int next_i = i + 1;
		while (next_i < len && name[next_i] == 'A')
			next_i++;
		int ln = len - next_i;
		int what = i + ln + min(i, ln);
		left_right = min(left_right, what);
	}
	answer += left_right;
	return answer;
}