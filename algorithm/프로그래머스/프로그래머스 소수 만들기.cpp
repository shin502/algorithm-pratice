#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<set>
#include<cmath>
using namespace std;
vector<int> ts;
bool check(int val) {
	if (1 == val) return false;

	for (int i = 2; i <= int(sqrt(val)); ++i) {
		if (val % i == 0) {
			return false;
		}
	}
	return true;

}

//조합 문제이다.
//
void dfs(int n , int next , int tt, vector<int>& nums) {
	if (n == 3) {		
		ts.emplace_back(tt);
		return;
	}

	for (int i = next; i < nums.size(); ++i) {
		dfs(n + 1, i + 1, tt + nums[i], nums);
	}
}
int main(void) {
	vector<int> nums = { 1,2,7,6,4 };
	int answer = 0;
	dfs(0, 0, 0,nums);
	for (auto a : ts) {
		if (true == check(a)) {
			answer++;
		}
	}
	return answer;
}