#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main(void) {//7 6 5 1 1 0
	vector<int> citations = { 1, 7, 0 , 1 , 6 , 4 };
	int answer = 0;
	sort(citations.begin(), citations.end(), [](int& a, int& b	) {
		return a > b;
		});

	int mv = -1;
	for (int i = 0; i < citations.size(); ++i) {
		if (citations[i] <= i) {
			return i;
		}
	}
}