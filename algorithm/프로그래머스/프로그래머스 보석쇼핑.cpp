#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<set>
#include<map>
#include<unordered_map>
using namespace std;


int main(void) {
	//vector<string> gems = {"DIA", "RUBY", "RUBY", "DIA", "DIA", "EMERALD", "SAPPHIRE", "DIA"};
	vector<string> gems = { "ZZZ", "YYY", "NNNN", "YYY", "BBB" };
	vector<int>answer;

	vector<pair<int, int>> ta;
	unordered_map<string,int> m;
	set<string> gs;
	for (int i = 0; i < gems.size(); ++i) {
		gs.insert(gems[i]);
	}
	int tt = m.size(); // 종류 사이즈
	int cnt = 0;
	int s = 0; int e = 0;
	while (true) {
		if (gs.size() == m.size()) {
			ta.emplace_back(s, e - 1);
			if (m[gems[s]] == s) {
				m.erase(gems[s]);
			}
			++s;
		}
		else {
			if (e == gems.size())
				break;

			m[gems[e]] = e;
			++e;
		}
	}
	sort(ta.begin(), ta.end(), []( pair<int,int>& a , pair<int,int>& b) {
		if (a.second - a.first < b.second - b.first)
			return true;
		else if (a.second - a.first == b.second - b.first) {
			return a.first < b.first;
		}
		return false;
	});

	answer.emplace_back(ta[0].first + 1);
	answer.emplace_back(ta[0].second + 1);



}