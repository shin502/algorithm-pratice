#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<sstream>
#include<string>
#include<set>
#include<cmath>

using namespace std;
vector<bool> visit;
vector<int> nvec;
set<int> ans;
int primeNum{ 0 };
bool isPrime(int n) {
	if (n == 0 || n == 1)
		return false;

	for (int i = 2; i <= int(sqrt(n)); ++i) {
		if (n % i == 0) {
			return false;
		}
	}
	return true;
}
void dfs(string number) {

	
	if (number != "" && isPrime(stoi(number)) == true) {
		if (ans.find(stoi(number)) == ans.end()) {
			primeNum++;
			ans.insert(stoi(number));
		}
	}

	for (int i = 0; i < nvec.size(); ++i) {
		if (visit[i] == false) {
			visit[i] = true;
			string temp = number;
			temp += to_string(nvec[i]);
			dfs(temp);
			visit[i] = false;

		}
	}

}


int main(void) {

	string numbers = "24";
	int answer = 0;
	for (int i = 0; i < numbers.size(); ++i) {
		char c = numbers[i];
		nvec.emplace_back(int(c - '0'));
	}
	visit.resize(nvec.size(), false);
	dfs("");
	answer = primeNum;
	cout << answer << endl;
	
}