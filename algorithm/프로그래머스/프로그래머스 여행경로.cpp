#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
string start = "ICN";
vector<vector<string>> fr;

void dfs(int n , string start , vector<vector<string>>& tickets , vector<bool>& visit , vector<string> t) {

	if (n == tickets.size()) {
		fr.emplace_back(t);
		return;
	}

	for (int i = 0; i < tickets.size(); ++i) {
		if (tickets[i][0] == start && visit[i] == false) {
			visit[i] = true;
			t.emplace_back(tickets[i][1]);
			dfs(n + 1, tickets[i][1], tickets, visit, t);
			t.pop_back();
			visit[i] = false;
		}
	}
	return;

}

int main(void) {
	vector<vector<string>> tickets = { {"ICN", "SFO"},{"ICN", "ATL"},{"SFO", "ATL"},{"ATL", "ICN"},{"ATL", "SFO"}};
	//vector<vector<string>> tickets = { {"ICN", "A"}, {"ICN", "A"}, {"A", "ICN"} };
	//vector<vector<string>> tickets = { { "ICN","A" }, { "A","B" }, { "B","A" }, { "A","ICN" }, { "ICN","A" } };
	vector<bool> visit;
	visit.resize(tickets.size(), false);
	vector<string> tt{ "ICN" };
	dfs(0, "ICN", tickets, visit, tt);
	sort(fr.begin(), fr.end());

	cout << fr.size() << endl;
}