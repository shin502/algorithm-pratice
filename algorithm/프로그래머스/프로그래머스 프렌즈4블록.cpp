#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;

int main(void) {
	//4개 붙어있는것은 없애야한다.
	int m = 4;
	int n = 3;
	//vector<string> board = { "CCBDE", "AAADE", "AAABF", "CCBBF" };
	//vector<string> board = {"TTTANT", "RRFACC", "RRRFCC", "TRRRAA", "TTMMMF", "TMMTTJ"};
	vector<string> board = { "KTT", "KKK", "KTT" , "TTT" };
	vector<vector<bool>> isExist;
	isExist.resize(m);
	for (int i = 0; i < isExist.size(); ++i) {
		isExist[i].resize(n, false);
	}
	int cnt = 0;
	while (true) {

		bool flag = false;

		//지우기 
		for (int i = 0; i < board.size() - 1; ++i) {
			for (int j = 0; j < board[i].size() - 1; ++j) {
				if (board[i][j] == board[i][j + 1] && board[i][j] == board[i + 1][j + 1] && board[i][j] == board[i + 1][j] && board[i][j] != '0') {
					isExist[i][j] = true;
					isExist[i][j + 1] = true;
					isExist[i + 1][j] = true;
					isExist[i + 1][j + 1] = true;
					flag = true;
				}
			}
		}
		//진짜 지움 
		for (int i = 0; i < isExist.size(); ++i) {
			for (int j = 0; j < isExist[i].size(); ++j) {
				if (isExist[i][j] == true && board[i][j] != '0') {
					board[i][j] = '0';
					cnt++;
				}
			}
		}
		

		if (!flag)
			break;
		//떨어뜨리기 
		for (int i = isExist.size() - 1; i > 0; --i) {
			for (int j = 0; j < isExist.size(); ++j) {
				if (board[i][j] == '0') {
					for (int k = i - 1; k >= 0; --k) {
						if (board[k][j] != '0') {
							board[i][j] = board[k][j];
							board[k][j] = '0';
							break;
						}
					}
				}
			}
		}

		for (int i = 0; i < isExist.size(); ++i) {
			for (int j = 0; j < isExist[i].size(); ++j) {
				if (board[i][j] == '0')
					isExist[i][j] = true;
				else
					isExist[i][j] = false;
			}
		}


	}
	cout << cnt << endl;
}