#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
using namespace std;

int main(void) {
	vector<int> people = { 70, 50, 80, 50 };
	int limit = 100;
	sort(people.begin(), people.end());
	int head = 0; int tail = people.size() - 1; int cnt = 0;
	while (true) {
		if (tail < head)
			break;
		if (people[head] + people[tail] <= limit) {
			head++;
			tail--;
		}
		else 
			tail--;
		
		cnt++;
	}
	return cnt;
}