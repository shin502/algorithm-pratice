#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

string bg;
string ed;
vector<string> wds;

vector<bool> visited;
int g_min = 10000000;

void dfs(int n , string& s) {
	if (s == ed) {
		g_min = min(g_min, n);
		return;
	}
	
	for (int i = 0; i < wds.size(); ++i) {
		if (visited[i] == true)
			continue;
		int cnt = 0;
		for (int j = 0; j < wds[i].size(); j++) {
			if (s[j] != wds[i][j]) {
				cnt++;
				if (cnt > 1)
					break;
			}
		}
		if (cnt == 1) {
			visited[i] = true;
			dfs(n + 1, wds[i]);
			visited[i] = false;
		}

	}
}


int main(void) {
	string begin = "hit";
	string end = "cog";
	vector<string> words = { "hot" , "dot" , "dog" , "lot" , "log" , "cog"};
	bg = begin;
	ed = end;
	wds = words;
	visited.resize(wds.size(), false);



	bool flag = false;
	for (int i = 0; i < words.size(); ++i) {
		if (words[i].compare(ed) == 0) {
			flag = true;
			break;
		}
	}

	if (flag == false)
		cout << 0 << endl;
	else {
		dfs(0, bg);
		cout << g_min << endl;
	}
}