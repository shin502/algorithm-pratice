//


//순열로 풀어야한다.
#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

int main(void) {
	int n = 12;
	vector<int> weak = {1,5,6,10};
	vector<int> dist = { 1,2};
	//vector<int> weak = { 1,3,4,9,10 };
	//vector<int> dist = { 3,5,7 };
	sort(dist.begin(), dist.end() , greater<int>()); // 순열을 위해서 dist를 sort 
	int answer = -1;
	/*
		weak 구조를 바꿔주면서 dist를 대입한다. 
		2 3 13
		2 13 14
		13 14 15
	*/
	for (int i = 0; i < weak.size(); ++i) {
		int tmp = weak[0] + n;
		for (int j = 1; j < weak.size(); ++j) {
			weak[j - 1] = weak[j];
		}
		weak[weak.size() - 1] = tmp;

		//순열을 한번 반복해야하므로 do while 문을 쓴다. 
		
		do {
			int w = 0;
			int d = 0;
			for (; d < dist.size(); ++d) { // 순열을 순회한다
				 int fin = weak[w] + dist[d];
				 while (fin >= weak[w]) { // 취약부분에다가 더한것이 넘어간다면 w를 더해준다 . 
					 ++w;
					 if (w == weak.size())
						 break;
				 }
				 if (w == weak.size()) // 취약부분을 다 보완한다면 break; 
					 break;
			}
			if (w == weak.size()) {
				if (answer == -1 || (d + 1) < answer) {
					answer = d + 1;
				}
			}



		} while (next_permutation(dist.begin(), dist.end()));

	}
	cout << answer << endl;


}