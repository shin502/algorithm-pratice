#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
//우선순위 큐로도 할수있고 max_elelment로도 할 수 있음
 
int main(void) {
	int n = 1;
	vector<int> works = {2,1,2};
	long long answer = 0;
	priority_queue<int> pq(works.begin(), works.end());
	for (int i = 0; i < n; ++i) {
		int mv = pq.top();
		if (mv == 0)	break;
		pq.pop();
		mv -= 1;  
		pq.push(mv);
	}
	while (!pq.empty()) {
		int tmp = pq.top();
		pq.pop();
		answer += tmp * tmp;
	}
	cout << answer << endl;
}