#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

int MOD = 20170805;

int main(void) {
	vector<vector<int>> city_map = {{0, 0, 0}, { 0, 0, 0 }, { 0, 0, 0 }};
	vector<vector<pair<int,int>>> visit;
	visit.resize(city_map.size());
	for (int i = 0; i < visit.size(); ++i) {
		visit[i].resize(city_map[i].size());
		for (int j = 0; j < visit[i].size(); ++j) {
			visit[i][j].first = 0; // 오른쪽
			visit[i][j].second = 0; // 아래쪽 
		}
	}
	int answer = 0;
	int row = city_map.size(); // 행 
	int column = city_map[0].size(); // 열 


	for (int i = 0; i < row; ++i) {
		if (city_map[i][0] == 1)
			break; //막혀버리니깐 continue가 아니라 break;
		visit[i][0].second = 1;
	}

	for (int i = 0; i < column; ++i) {
		if (city_map[0][i] == 1)
			break;
		visit[0][i].first = 1;
	}
	
	for (int i = 1; i < row; ++i) {
		for (int j = 1; j < column; ++j) {

			//위쪽 에서 내려온거 
			if (city_map[i - 1][j] == 0) {
				visit[i][j].second += (visit[i - 1][j].second + visit[i - 1][j].first) % MOD;
			}
			else if (city_map[i - 1][j] == 2) {
				visit[i][j].second += (visit[i - 1][j].second) % MOD;
			}

			//왼쪽에서 온거 
			if (city_map[i][j - 1] == 0) {
				visit[i][j].first += (visit[i][j - 1].second + visit[i][j - 1].first) % MOD; 
			}
			else if (city_map[i][j - 1] == 2) {
				visit[i][j].first += (visit[i][j - 1].first) % MOD;
			}

		}
	}
	int answer = (visit[row - 1][column - 1].first + visit[row - 1][column - 1].second) % MOD;
	return answer;

}