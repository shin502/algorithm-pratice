#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
bool check(string p) {
	bool flag = true;
	int cnt = 0;
	for (int i = 0; i < p.size(); ++i)
	{
		if (p[i] == '(') {
			cnt++;
		}

		if (cnt > 0 && p[i] == ')') {
			cnt--;
		}
		else if (cnt <= 0 && p[i] == ')') {
			//올바른 괄호 문자열이 아님
			flag = false;
		}
	}
	return flag;
	

}
string recursion(string p) {

	if (p == "") return p;

	if (check(p)) {
		return p;
	}

	//균형 잡힌 문자열 두개로 분리
	int rc = 0;
	int lc = 0;
	string u;
	string v;
	for (int i = 0; i < p.size(); ++i) {
		if (p[i] == '(')
			++lc;
		else
			++rc;
		if (lc == rc) {
			u = p.substr(0, i + 1);
			v = p.substr(i + 1);
			break;
		}
	}
	//다른 문자는 재귀
	if (check(u))
		return u + recursion(v);

	string temp = "";
	temp += "(";
	temp += recursion(v) + ")";

	string nu = u.substr(1, u.length() - 2);
	for (int i = 0; i < nu.size(); ++i) {
		if (nu[i] == '(')
			nu[i] = ')';
		else
			nu[i] = '(';
	}

	temp += nu;
	return temp;


}

int main(void) {
	string answer;
	string p = ")()()()(";

	//올바른 괄호 문자열인지 판단
	answer = recursion(p);
	cout << answer << endl;
}