#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main(void) {
	string name = "JEROEN";
	
	string ts;
	vector<bool> tb;
	tb.resize(name.length(), false);
	for (int i = 0; i < name.size(); ++i) {
		ts += "A";
		if (name[i] != 'A')
			tb[i] = true;
	}

	int answer = 0;
	//A가 있느냐 없느냐에따라서 오른쪽으로 갈지 왼쪽으로 갈지 선택?

	//바꾼 횟수를 추가 

	int cp = 0;
	int cnt = 0;
	int len = name.length();
	int dir = 0;
	int dirSize = 0;


	while (1) {
		//문자와 바꿔준다.
		if (dir == 0) {
			if (ts[cp] != name[cp]) {
				if (name[cp] < 78) {// A~M까지 
					cnt += name[cp] % 65;
				}
				else { // N~Z
					cnt += 91 - name[cp];
				}
				ts[cp] = name[cp];
				tb[cp] = false;
				dir = 1;
			}
		}
		else
		{

			if (name[cp] < 78) {// A~M까지 
				cnt += name[cp] % 65;
			}
			else { // N~Z
				cnt += 91 - name[cp];
			}
			ts[cp] = name[cp];
			tb[cp] = false;
			dir = 1;
		}

		if (name == ts)
			break;
		//오른쪽으로 갈지 왼쪽으로 갈지 -> 오른쪽으로가는게 A가 더 많은지 왼쪽으로가는게 A가 더 많은지 확인 

		//오른쪽
		int right = 0;
		int rp = cp;
		while (1) {
			if (tb[rp] == false) {
				right++;
			}
			else {
				break;
			}
			rp++;
			if (rp == name.size())
				rp = 0;
		}
		int lp = cp;
		int left = 0;
		while (1) {
			if (tb[lp] == false) {
				left++;
			}
			else {
				break;
			}
			lp--;
			if (lp == -1)
				lp = name.size() - 1;
		}
		
		if (left >= right) {
			cp = rp;
			cnt += right;
		}
		else {
			cp = lp;
			cnt += left;
		}
	}
	answer = cnt;
	return answer;


}