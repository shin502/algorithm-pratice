#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
using namespace std;
//BFS 너비우선 탐색으로 풀어야한다.
//4방향으로 회전한 것을 넣어줘야한다.

bool visit[101][101][4] = { false };

int mx[4] = { 1, 0 , -1, 0 }; // 오른쪽 ,아래 , 왼쪽 , 위 
int my[4] = { 0, 1, 0 , -1 };

int mrx[4] = { 1,1,-1,-1 };
int mry[4] = { -1,1,1,-1 };

int omrx[4] = { 1,-1,-1,1 };
int omry[4] = { 1,1 ,-1,-1 };

int destx;
int desty;


bool isCheck(int nx , int ny , int dir , vector<vector<int>>& vec) {
	if (nx < 0 || nx > destx || ny < 0 || ny > desty)
		return false;
	if (vec[ny][nx] == 1)
		return false;

	return true;

}
bool deCheck(int nx1, int ny1, int nx2, int ny2) {
	if (nx1 == destx && ny1 == desty)
		return true;
	if (nx2 == destx && ny2 == desty)
		return true;

	return false;
}

struct Robot {
	int dir;
	int x;
	int y;
	int time;
	Robot(int x, int y, int dir ,int time) {
		this->x = x;
		this->y = y;
		this->dir = dir; // dir 은 x 와 y 좌표에서 어느 방향 인지이다. 앞에가 x좌표 뒤에가 y 좌표로 계산해야한다.
		this->time = time;
	}
};
int main(void) {
	//vector<vector<int>> board = { {0,0,0,1,1} , {0,0,0,1,0} , {0,1,0,1,1} , {1,1,0,0,1} , {0,0,0,0,0} };
	//vector<vector<int>> board = { {0, 0, 0, 0, 1, 0}, {0, 0, 1, 1, 1, 0}, {0, 1, 1, 1, 1, 0}, {0, 1, 0, 0, 1, 0}, {0, 0, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0} };
	//vector<vector<int>> board = {{0, 0, 0, 0, 0, 0, 1}, {1, 1, 1, 1, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 0}, {0, 0, 1, 1, 1, 1, 0}, {0, 1, 1, 1, 1, 1, 0}, {0, 0, 0, 0, 0, 1, 1}, {0, 0, 1, 0, 0, 0, 0}};
	vector<vector<int>> board = { {0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 1, 1, 1, 1, 1, 0, 0}, {0, 1, 1, 1, 1, 1, 1, 1, 1}, {0, 0, 1, 1, 1, 1, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 0} };
	//vector <vector<int>> board = { {0, 0, 1, 1, 1, 1, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 0} };

	int answer = 0;
	
	destx = board.size() - 1;
	desty = board.size() - 1;



	Robot sr{ 0, 0, 0, 0 };
	queue<Robot> q;
	visit[0][0][0] = true;

	q.push(sr);
	while (!q.empty()) { // 큐가 비지 않을 동안 계속 돌린다 
		Robot r = q.front();
		q.pop();
		int x1 = r.x;
		int y1 = r.y;
		int dir = r.dir; // 방향은 4가지 
		int x2 = x1 + mx[dir]; // 배열에다가 dir을 넣어줘서 위치를 바로 알아낸다.
		int y2 = y1 + my[dir];
		int ct = r.time;

		if (deCheck(x1, y1, x2, y2)) {
			answer = ct;
			break;
		}

		//좌표와 방향을 알아냈다.
		//4가지 방향으로 전진 시킨다.
		int nx1;
		int ny1;
		int nx2;
		int ny2;

		//이동 
		for (int i = 0; i < 4; ++i) { // 오른쪽 , 아래 , 왼쪽 , 위 
			nx1 = x1 + mx[i];
			nx2 = x2 + mx[i];

			ny1 = y1 + my[i];
			ny2 = y2 + my[i];
			
			bool flag = true;
			//검사 
			flag = isCheck(nx1, ny1 ,dir, board);
			if (!flag)
				continue;
			flag = isCheck(nx2, ny2 ,dir, board);
			if (!flag)
				continue;
			
			if (visit[ny1][nx1][dir] == true)
				continue;


			visit[ny1][nx1][dir] = true;
			q.push(Robot(nx1, ny1, dir ,ct + 1 ));
		}

		//회전
		//시계 , 반시계 

		//x1 , y1이 기준 시계 반시계 
		for (int i = 1; i < 4; i += 2) {
			int ndir = (dir + i) % 4;
			int nx1 = x1;
			int ny1 = y1;
			int nx2;
			int ny2;

			int rx;
			int ry;

			if (i == 1)//시계
			{
				rx = nx1 + mrx[ndir];
				ry = ny1 + mry[ndir];
			}
			else if (i == 3) 
			{ // 반시계
				rx = nx1 + omrx[ndir];
				ry = ny1 + omry[ndir];
			}
			nx2 = x1 + mx[ndir];
			ny2 = y1 + my[ndir];

			bool flag = true;
			//검사 
			flag = isCheck(nx1, ny1, ndir, board);
			if (!flag) 
				continue;
			flag = isCheck(nx2, ny2, ndir, board);
			if (!flag) 
				continue;
			
			if (board[ry][rx] == 1)	continue;

			if (visit[ny1][nx1][ndir] == true)
				continue;

			visit[ny1][nx1][ndir] = true;
			q.push(Robot(nx1, ny1, ndir, ct + 1));

		}


		//x2 , y2가 기준 시계 반시계
		dir = (dir + 2) % 4;
		for (int i = 1; i < 4; i += 2) {
			int ndir = (dir + i) % 4;
			int nx1 = x2;
			int ny1 = y2;
			int nx2;
			int ny2;

			int rx;
			int ry;

			nx2 = nx1 + mx[ndir];
			ny2 = ny1 + my[ndir];
			if (i == 1)//시계
			{
				rx = nx1 + mrx[ndir];
				ry = ny1 + mry[ndir];
			}
			else if (i == 3)
			{ // 반시계
				rx = nx1 + omrx[(ndir)];
				ry = ny1 + omry[(ndir)];
			}
			bool flag = true;
			//검사 
			flag = isCheck(nx1, ny1, ndir, board);
			if (!flag)
				continue;
			flag = isCheck(nx2, ny2, ndir, board);
			if (!flag)
				continue;

			if (board[ry][rx] == 1)	continue;

			if (visit[ny1][nx1][ndir] == true)
				continue;

			visit[ny1][nx1][ndir] = true;
			q.push(Robot(nx1, ny1, ndir, ct + 1));

		}

	}
	cout << answer << endl;

}