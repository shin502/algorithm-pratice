#include <iostream>
#include <string>
#include <vector>
#include <cmath>
using namespace std;
vector<vector<int>> network;
int computer[3][3] = {{1,1,0},
					  {1,1,0},
					  {0,0,1}};

//i번과 j번이 연결되어있으면 1로표시
//01 -> 10 01 02 20 
//연쇄적으로 일어나면 어떻게 되는지를 생각해보자.
//1하고 2 연결  2하고 3하고 연결 
// 4
// 1 2 1 3 4 4 2
// 1 2 2 3 

int n = 3;
int g_count = n;
void dfs(int cnt) {
	//탐색함
	if (computer[cnt][cnt] == 0)
		return;
	//탐색완료 표시
	computer[cnt][cnt] = 0;
	for (int i = 0; i < n; ++i) {
		if (computer[cnt][i] == 1)
			dfs(i);
	}
}

int main(void) {	
	
	int answer = 0;
	for (int i = 0; i < n; ++i) {
		if (computer[i][i] == 1) {
			dfs(i);
			++answer;
		}
	}

	cout << answer;

}