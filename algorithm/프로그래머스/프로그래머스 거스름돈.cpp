#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
//dp로 풀어야한다.
//dp로 점화식을 유도하려면 테이블을 만들어야한다.

int MOD = 1000000007;

int main(void) {
	int n = 5;
	vector<int> money = { 1,2,5 };
	vector<int> ways(n + 1);
	ways[0] = 1;

	//ways[coin] += ways[m - coin]
	for (auto coin : money) {
		for (int j = coin; j < ways.size(); ++j) {
			ways[j] += ways[j - coin];
			ways[j] %= MOD;
		}
	}

	return ways[n];
}