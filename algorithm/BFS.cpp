#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
using namespace std;

constexpr int MAX = 10;
vector<int> graph[MAX];
void BFS() {
	queue<int> q;
	q.push(0);

	while (!q.empty()) {
		int x = q.front();
		cout << x << endl;
		q.pop();
		for (int i = 0; i < graph[x].size(); ++i) {
			q.push(graph[x][i]);
		}
	}

}

int main(void) {
	//BFS
	graph[0].push_back(1);
	graph[0].push_back(2);
	graph[1].push_back(3);
	graph[1].push_back(4);
	graph[2].push_back(5);
	graph[2].push_back(6);
	BFS();
}