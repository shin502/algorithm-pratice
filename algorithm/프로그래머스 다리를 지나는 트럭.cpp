#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
using namespace std;

int main(void) {
	int bridge_length = 2;
	int weight = 10; //다리가 견딜수 있는 무게
	vector<int> truck_weights = { 7,4,5,6 };
	queue<int> tqu;
	for (int i = 0; i < truck_weights.size(); ++i) {
		tqu.push(truck_weights[i]);
	}

	queue<pair<int,int>> bridge_truck;
	int btweight = 0;
	int time = 1;

	while (true) {
		
		while (!bridge_truck.empty()) {
			pair<int, int> tmp = bridge_truck.front();
			if (time - tmp.second >= bridge_length)
			{
				bridge_truck.pop();
				btweight -= tmp.first;
			}
			else
				break;
		}

		if (bridge_truck.empty() && tqu.empty())
			break;

		//트럭을 다리 위로 배치
		
		//트럭 무게를 받아봄
		if (!tqu.empty()) {
			int tw = tqu.front();
			if (tw + btweight <= weight) {//다리위로 배치 가능하다면 
				tqu.pop();
				bridge_truck.push(make_pair(tw, time)); //배치 
				btweight += tw;
			}
		}
		//time 증가 
		time++;



	}


	return time;
}