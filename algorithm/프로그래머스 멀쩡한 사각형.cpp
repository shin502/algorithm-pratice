#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<math.h>
using namespace std;

//최대공약수로 풀기 

//int gcd(int a, int b) {
//	int c;
//	while (b != 0) {
//		c = a % b; // 4 6 6 4 4 2  
//		a = b; // a = 6 a = 4 -> a = 2
//		b = c; // b = 4 b = 2 b -> 2
//	}
//	return a;
//}
//
//
//int main(void) {
//	int w = 8;
//	int h = 12;
//	long long g = gcd(w, h);
//	long long lw = w;
//	long long lh = h;
//
//	long long answer = lw * lh - (lw + lh - g);
//	return answer;
//	//둘을 더해서 최대 공약수를 빼주면 된다
//}

// 직선의 방정식으로 풀기

int main(void) {
	int w = 8;
	int h = 12;
	long long lh = h;
	double dw = w;
	double dh = h;
	double lean = dh / dw;
	long long answer = 0;
	for (int i = 1; i <= w; ++i) {
		long long pos = ceil(lean * double(i));
		answer += long long(h - pos);


	}
	return answer * 2;

}