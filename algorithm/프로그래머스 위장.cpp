#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<unordered_map>
using namespace std;
vector<vector<bool>> visit;
int tt = 0;
void dfs(int n) {

	if (n == visit.size()) {
		return;
	}

	for (int j = n; j < visit.size(); ++j) {
		for (int i = 0; i < visit[j].size(); ++i) {
			if (visit[j][i] == true)
				continue;
			visit[j][i] = true;
			++tt;
			dfs(j + 1);
			visit[j][i] = false;
		}
	}
}

int main(void) {
	vector<vector<string>> clothes = { {"a", "a"},{"b", "b"},{"c", "c"}};

	unordered_map<string, int> um;
	for (int i = 0; i < clothes.size(); ++i) {
		um[clothes[i][1]] += 1;
	}	
	visit.resize(um.size());

	int cnt = 0;
	for (auto& a : um) {
		visit[cnt++].resize(a.second);
	}
	dfs(0);
	return tt;

}