#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;

//100
//40 40 50 60
//10 10 100 100
//50 70 80
int main(void) {
	vector<int> people = { 70, 80, 50 };
	int limit = 100;

	sort(people.begin(), people.end());
	int cnt = 0;
	int len = people.size() - 1;
	int front = 0;
	while (front <= len) {
		if ((len - front) + 1 >= 2) {
			if (people[len] + people[front] <= limit)
			{
				front += 1;
				len -= 1;
			}
			else {
				len -= 1;
			}
		}
		else {
			len -= 1;
		}
		cnt++;
	}

	return cnt;
}