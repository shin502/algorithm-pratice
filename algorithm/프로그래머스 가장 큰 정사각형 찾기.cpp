#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
//DP로 풀어야하는 문제이다.
int main(void) {
	vector<vector<int>> board = { {1, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0} };
	int mv = 0;
	for (int i = 0; i < board.size(); ++i) {
		for (int j = 0; j < board[i].size(); ++j) {
			mv = max(mv, board[i][j]);
			if (board[i][j] == 0 || i - 1 < 0 || j - 1 < 0)
				continue;
		
			int val = min(board[i - 1][j], board[i][j - 1]);
			val = min(val, board[i - 1][j - 1]);
			board[i][j] = val + 1;
			mv = max(mv, board[i][j]);
		}
	}
	
	return mv * mv;


}