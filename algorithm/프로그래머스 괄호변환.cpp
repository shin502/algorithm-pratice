#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
bool isRight(string u) {
	int left = 0;
	int right = 0;

	for (int i = 0; i < u.size(); ++i) {
		if (right < left)
			return false;

		if (u[i] == '(') right++;
		else if (u[i] == ')') left++;
	}

	return true;
}
string dfs(string p) {

	if (p == "")
		return "";

	int right = 0;
	int left = 0;
	string u;//분리 불가능
	string v;//분리 가능 
	string newStr = "";
	for (int i = 0; i < p.size(); ++i) {
		if (p[i] == '(') {
			right++;
		}
		else if (p[i] == ')') {
			left++;
		}

		if (right == left) {
			u = p.substr(0, i + 1);
			v = p.substr(i + 1);
			break;
		}
	}
	//올바른 문자열이라면 
	if (true == isRight(u)) {
		newStr += u;
		newStr += dfs(v);

		return newStr;
	}
	else {
		string temp = "(";
		u.erase(u.begin());
		u.pop_back();
		for (auto& a : u) {
			if (a == '(')	a = ')';
			else if (a == ')')	a = '(';
		}
		temp += dfs(v);
		temp += ")";

		return temp + u;
	}

}

int main(void) {
	string p = "()))((()";
	string newStr = "";
	newStr = dfs(p);

	cout << newStr << endl;
	



}