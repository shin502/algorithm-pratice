#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;

struct P {
	int idx;
	int val;

	
};

struct compare {
	bool operator()(P& a, P& b) {
		return a.val < b.val;
	}
};

int main(void) {
	vector<int> prices = {1, 2, 3 ,2, 3};
	vector<int> answer;
	answer.resize(prices.size());
	priority_queue<P, vector<P> , compare> qu;


	int time = 0;
	bool isDown = false;
	for (int i = 0; i < prices.size(); ++i) {
		while (!qu.empty()) {
			P temp = qu.top();
			if (temp.val > prices[i]) {
				qu.pop();
				answer[temp.idx] = i - temp.idx;
			}
			else
				break;
		}
		//Ǫ��
		P p;
		p.idx = i;
		p.val = prices[i];
		qu.push(p);

	}
	while (!qu.empty()) {
		P temp = qu.top();
		qu.pop();
		answer[temp.idx] = (prices.size() -1) - temp.idx;
	}

}