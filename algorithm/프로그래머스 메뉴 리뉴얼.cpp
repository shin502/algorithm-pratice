//#include<iostream>
//#include<algorithm>
//#include<string>
//#include<vector>
//using namespace std;
//
////조합 
//vector<pair<string,int>> cb;
//vector<string> od;
//
//void dfs(int n, int limit ,int next ,string s ,vector<int>& pm) {
//	if (n == limit) {
//		int sameCnt = 0;
//		for (int i = 0; i < od.size(); ++i) {
//			bool flag = false;
//			for (int j = 0; j < s.size(); ++j) {
//				if (od[i].find(s[j]) == std::string::npos) {
//					flag = true;
//					break;
//				}
//			}
//			if (flag == false)
//				sameCnt++;
//		}
//		if (sameCnt < 2)
//			return;
//
//		if (cb.empty())
//			cb.emplace_back(make_pair(s, sameCnt));
//		else
//		{
//			if (cb[0].second <= sameCnt) {
//				cb.emplace_back(make_pair(s, sameCnt));
//				sort(cb.begin(), cb.end(), [](const pair<string, int>& a, const pair<string, int>& b) {
//					return a.second > b.second;
//				});
//			}
//		}
//
//		return;
//	}
//
//
//
//	for (int i = next; i < pm.size(); ++i) {
//		if (pm[i] == 0)
//			continue;
//		if (s.size() < limit) {
//			s += 'A' + i;
//			dfs(n + 1, limit, i + 1, s, pm);
//			s.pop_back();
//		}
//	}
//
//}
//
//
//int main(void) {
//	//unordered_map에다가 숫자개수를 저장한다
//	//최소 2명이상 주문해야한다.
//	//->  2두번쨰로 높은숫자 3번째로 높은숫자 순으로 
//	//알파벳 정렬은 되어야한다.
//
//	vector<string> orders = { "ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH" };
//	vector<int> course = { 2,3,4 };
//
//	od = orders;
//	vector<int> pm;
//	pm.resize(26 , 0);
//	
//
//
//	for (int i = 0; i < orders.size(); ++i) {
//		for (int j = 0; j < orders[i].size(); ++j) {
//			pm[orders[i][j] - 'A'] = 1;
//		}
//	}
//	
//	vector<string> answer;
//	for (int i = 0; i < course.size(); ++i) {
//		cb.clear();
//		dfs(0,course[i] ,0,"",pm); //코스의 개수 
//		for (int j = 0; j < cb.size(); ++j) {
//			answer.emplace_back(cb[j].first);
//			if (j != cb.size() - 1 && cb[j].second != cb[j + 1].second)
//				break;
//		}
//	}
//
//	sort(answer.begin(), answer.end());
//
//
//}

#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<unordered_map>
using namespace std;
int cnt[27]; // cnt[i]=n : 길이가 i인 조합 중 최대 주문 횟수는 n번
unordered_map<string, int> um; // um[str]=n : 조합 str의 주문 횟수는 n번
vector<string> menu[27][21]; // menu[i][j] : 길이가 i이고 j번 주문된 조합들의 목록

void comb(string s, int idx, string made) {
    if (made.size() > 1) { // 2번 이상 주문된 것만
        um[made]++; // 해당 조합 주문 횟수 누적
        cnt[made.size()] = max(cnt[made.size()], um[made]); // 조합 길이 별 최대 주문 횟수 갱신
        menu[made.size()][um[made]].push_back(made); // 분류별 조합 목록 추가
    }
    // 백트래킹
    for (int i = idx + 1; i < s.size(); i++) {
        made.push_back(s[i]);
        comb(s, i, made);
        made.pop_back();
    }
}


int main(void) {
	vector<string> orders = { "ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH" };
	vector<int> course = { 2,3,4 };

    vector<string> answer;

	for (string& s : orders) {
		sort(s.begin(), s.end()); // 순서만 다르고 중복된 조합이 나오지 않도록 정렬
		comb(s, -1, "");
	}

    // 쿼리 수행
    for (int i : course)
        if (cnt[i] > 1) // 길이가 i인 조합의 최대 주문 횟수가 1 이상인 경우만
            for (string s : menu[i][cnt[i]])
                answer.push_back(s);

    sort(answer.begin(), answer.end());

   // return answer;

}