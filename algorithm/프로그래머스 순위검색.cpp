//#include<iostream>
//#include<algorithm>
//#include<vector>
//#include<string>
//using namespace std;
//vector<int> person[3][2][2][2];
//
//int changeIndx(char c, int number) {
//	if (number == 0) {
//		if (c == 'c')
//			return 0;
//		else if (c == 'j')
//			return 1;
//		else if (c == 'p')
//			return 2;
//		else if (c == '-')
//			return 5;
//	}
//	else if (number == 1) {
//		if (c == 'b')
//			return 0;
//		else if (c == 'f')
//			return 1;
//		else if (c == '-')
//			return 5;
//	}
//	else if (number == 2) {
//		if (c == 'j')
//			return 0;
//		else if (c == 's')
//			return 1;
//		else if (c == '-')
//			return 5;
//	}
//	else if (number == 3) {
//		if (c == 'c')
//			return 0;
//		else if (c == 'p')
//			return 1;
//		else if (c == '-')
//			return 5;
//	}
//
//	return 0;
//}
//
//
//int main(void) {
//	vector<string> info = { "java backend junior pizza 150", "python frontend senior chicken 210", "python frontend senior chicken 150", "cpp backend senior pizza 260", "java backend junior chicken 80", "python backend senior chicken 50" };
//	vector<string> query = {"java and backend and junior and pizza 100", "python and frontend and senior and chicken 200", "cpp and - and senior and pizza 250", "- and backend and senior and - 150", "- and - and - and chicken 100", "- and - and - and - 150"};
//	
//	vector<int> answer;
//	
//	
//	for (int i = 0; i < info.size(); ++i) {
//		int prv = 0;
//		int cnt = 0;
//		int tmp[5];
//		for (int j = 0; j < info[i].size(); ++j) {
//			if (info[i][j] == ' ') {
//				string s = info[i].substr(prv, j - prv);
//				tmp[cnt] = changeIndx(s[0], cnt);
//				cnt++;
//				prv = j + 1;
//			}
//		}
//		tmp[cnt] = stoi(info[i].substr(prv));
//		person[tmp[0]][tmp[1]][tmp[2]][tmp[3]].emplace_back(tmp[cnt]);
//	}
//
//	for (int i = 0; i < 3; ++i) {
//		for (int j = 0; j < 2; ++j) {
//			for (int k = 0; k < 2; ++k) {
//				for (int l = 0; l < 2; ++l) {
//					sort(person[i][j][k][l].begin(), person[i][j][k][l].end());
//				}
//			}
//		}
//	}
//
//
//	for (int i = 0; i < query.size(); ++i) {
//		int prv = 0;
//		int cnt = 0;
//		int tmp[5];
//		int is[4] = { 0, 0,0,0 };
//		int ie[4] = { 0,0,0,0 };
//		for (int j = 0; j < query[i].size(); ++j) {
//			if (query[i][j] == ' ') {
//				string s = query[i].substr(prv, j - prv);
//				if (s == " and" || s == "and") {
//					prv = j + 1;
//					continue;
//				}
//				tmp[cnt] = changeIndx(s[0], cnt);
//				if (tmp[cnt] == 5) {
//					if (cnt == 0) {
//						is[cnt] = 0;
//						ie[cnt] = 3;
//					}
//					else {
//						is[cnt] = 0;
//						ie[cnt] = 2;
//					}
//				}
//				else {
//					is[cnt] = tmp[cnt];
//					ie[cnt] = tmp[cnt] + 1;
//				}
//				cnt++;
//				prv = j + 1;
//			}
//		}
//		tmp[cnt] = stoi(query[i].substr(prv));
//		
//		int tt = 0;
//		for (int r = is[0]; r < ie[0]; ++r) {
//			for (int t = is[1]; t < ie[1]; ++t) {
//				for (int y = is[2]; y < ie[2]; ++y) {
//					for (int u = is[3]; u < ie[3]; ++u) {
//						auto iter = lower_bound(person[r][t][y][u].begin(), person[r][t][y][u].end(), tmp[4]);
//						if (iter == person[r][t][y][u].end())
//							continue;
//						tt += person[r][t][y][u].size() - (iter - person[r][t][y][u].begin());
//
//					}
//				}
//			}
//		}
//		answer.emplace_back(tt);
//	}
//	cout << answer[0] << endl;
//
//}

#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <sstream>

using namespace std;

const string ALL = "-";
unordered_map<string, vector<int>> map;


void insert(string* key, int mask, int point) {
    string s = "";
    for (int i = 0; i < 4; i++) {
        s += (mask & (1 << i)) ? ALL : key[i];
        map[s].push_back(point);
    }
}

int main(void) {
    vector<string> info = { "java backend junior pizza 150", "python frontend senior chicken 210", "python frontend senior chicken 150", "cpp backend senior pizza 260", "java backend junior chicken 80", "python backend senior chicken 50" };
    vector<string> query = { "java and backend and junior and pizza 100", "python and frontend and senior and chicken 200", "cpp and - and senior and pizza 250", "- and backend and senior and - 150", "- and - and - and chicken 100", "- and - and - and - 150" };

    vector<int> answer;
    string key[4], tmp;
    int point;

    for (auto& inf : info) {
        istringstream iss(inf);
        iss >> key[0] >> key[1] >> key[2] >> key[3] >> point;
        for (int i = 0; i < 16; i++) insert(key, i, point);
    }

    for (auto& m : map) sort(m.second.begin(), m.second.end());

    for (auto& que : query) {
        istringstream iss(que);
        iss >> key[0] >> tmp >> key[1] >> tmp >> key[2] >> tmp >> key[3] >>
            point;
        string s = key[0] + key[1] + key[2] + key[3];
        vector<int>& v = map[s];
        answer.push_back(v.end() - lower_bound(v.begin(), v.end(), point));
    }
}