#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
int tt = 0;
void dfs(int n, int number, vector<int>& numbers, int target) {

	if (number == target && n == numbers.size())
	{
		tt += 1;
	}

	if (n >= numbers.size())
		return;

	dfs(n + 1, number + numbers[0], numbers, target);
	dfs(n + 1, number - numbers[0], numbers, target);
}

int main(void) {
	vector<int> numbers = {1,1,1,1,1};
	int target = 3;

	dfs(0 , 0 , numbers , target);
	return tt;
}