#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<queue>
using namespace std;

int main(void) {
	vector<int> progresses = { 95,90,99,99,80,99 };;
	vector<int> speeds = { 1,1,1,1,1,1 };
	queue<int> qu;
	vector<int> answer;
	for (auto a : progresses)
		qu.push(a);
	
	int idx = 0;
	int time = 0;

	while (!qu.empty()) {

		int fval = 0;
		while (!qu.empty()) {
			int working = qu.front();
			working += time * speeds[idx];
			if (working >= 100) {
				qu.pop();
				idx++;
				fval++;
			}
			else
				break;
		}
		if (fval != 0)
			answer.emplace_back(fval);


		time++;
	}

	cout << answer[0] << endl;
}