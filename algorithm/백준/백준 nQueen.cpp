#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
int n;
int total = 0;
int answer = 0;
//첫번째 줄에 퀸을 놓았을때 -> 백트래킹하면서 탐색 좌우 , 대각선에 있으면 백트래킹 빼주고 빽트래킹
struct Pon {
	int x;
	int y;
	Pon(int x , int y) : x(x) , y(y) {}
};
vector<Pon> vec;

int cnt = 0;
void nQueen(int y , int x) {
	

		for (int i = 0; i < vec.size(); ++i) {
			if (vec[i].x == (x)) {
				return;
			}
			if ((vec[i].x + vec[i].y) == (x + y) || (vec[i].x - vec[i].y) == (x - y)) {
				return;
			}
		}

		if (y == n - 1) { // 통과 
			answer++;
			return;
		}


		vec.emplace_back(Pon(x, y));
		for (int i = 0; i < n; ++i) {
			nQueen(y + 1, i);
		}
		vec.pop_back();
		
}

int main(void) {
	

	cin >> n;
	for (int i = 0; i < n; ++i) {
		nQueen(0, i);
	}

	cout << answer;
	return 0;

}