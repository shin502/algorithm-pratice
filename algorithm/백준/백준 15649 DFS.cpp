#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
using namespace std;

constexpr int MAX = 8 + 1;

int val[MAX]; // 출력할 리스트
bool visited[MAX]; // 방문 여부 

int n, m;
void dfs(int cnt) {
	// 길이랑 cnt 값이 같다면 출력하고 백트래킹

	if (cnt == m) { // 출력 조건 
		for (int i = 0; i < m; ++i) {
			cout << val[i] << " ";
		}
		cout << "\n";
		return;
	}

	//카드를 뽑는 곳 
	for (int i = 1; i <= n; ++i) { // n까지 출력하는거다.
		if (!visited[i]) {// 앞에서 1을 뽑았으면 뒤에서 1을 못뽑게 하기위해서 있는거다.
			visited[i] = true;
			val[cnt] = i; // 3 넣고 나면 4에 VISITED가 풀려있으므로 CNT 2번자리에 4를 넣을수 있다 그런식으로 흘러간다.
			dfs(cnt + 1); //길이를 1 올려줌 길이가 m하고 같아지면 출력 
			visited[i] = false;// 뽑았던걸 출력했으면 다시 헤제
		}

	}

}
//visited는 뭘까
//뽑은걸 또 못 뽑게 해야한다.
//
int main(void) {

	cin >> n >> m;
	// n 은 숫자 리미트 , m은 길이 
	dfs(0);
	
	return 0;
}