#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
int n;
constexpr int MAX = 20;
int arr[MAX][MAX];
bool isused[MAX];

int minval = 10000000;


// n / 2로 갈리도록 선수들을 뽑아야함 
void dfs(int cnt , int index) {
	//선수를 다 뽑으면 스타트 선수는 true 나머지는 링크팀 
	if (cnt == n / 2) {
		vector<int> startvec;
		vector<int> linkvec;

		for (int i = 0; i < n; ++i) {
			if (isused[i] == true)
				startvec.emplace_back(i);
			else
				linkvec.emplace_back(i);
		}
		int stSum = 0;
		int ktSum = 0;
		for (int i = 0; i < startvec.size(); ++i) {
			for (int j = i + 1; j < startvec.size(); ++j) {
				int sf = startvec[i];
				int ss = startvec[j];

				int kf = linkvec[i];
				int ks = linkvec[j];

				stSum += arr[sf][ss] + arr[ss][sf];
				ktSum += arr[kf][ks] + arr[ks][kf];
			}
		}

		int finalval = abs(stSum - ktSum);
		minval = min(minval, finalval);

		return;
	}


		//선수뽑기
	for (int i = index + 1; i < n; ++i) {//index + 1 을 해준것은 가지치기이다. 원래 1 , 2 1 ,3 을하고나서 2 , 1 이렇게 들어갔느데 
		//index + 1을 해주면 자기보다 큰 수가 들어가지않느다.
		if (isused[i] == false) {
			isused[i] = true;
			dfs(cnt + 1, i);
			isused[i] = false;
		}
	}

	return;

}


int main(void) {
	cin >> n;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cin >> arr[i][j];
		}
	}

	dfs(0 , 0);
	cout << minval << "\n";

}