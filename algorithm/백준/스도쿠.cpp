#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;
constexpr int SDQ = 9;



int board[SDQ][SDQ];
int cnt = 0;
vector<pair<int, int>> gblank;//재귀에서 굳이 행렬을 찾을 필요없이 여기다가 저장한다.
int g_end = 0;

void dfs(int val , int num) {
	if (g_end == 1)
		return;

	//완료 조건 검사 
	if (val == cnt) {
		for (int i = 0; i < SDQ; ++i) {
			for (int j = 0; j < SDQ; ++j) {
				cout << board[i][j] << " ";
			}
			cout << "\n";
		}
		g_end = 1;
		return;
	}
	//숫자를 넣어도 되는지 검사 -> 가로 , 세로 , 9x9에 같은 숫자가 없으면 그 숫자를 넣는다.
	if (val != -1) {
		int first = gblank[val].first;
		int second = gblank[val].second;
			bool isInsert = true;
			//세로
			for (int i = 0; i < 9; ++i) {
				if (board[i][second] == num) {
					board[first][second] = 0;
					return;
				}
			}
			//가로
			for (int i = 0; i < 9; ++i) {
				if (board[first][i] == num) {
					board[first][second] = 0;
					return;
				}
			}
			//3x3
			int tf = (first / 3) * 3;
			int ts = (second / 3) * 3;
			for (int i = tf; i < tf + 3; ++i) {
				for (int j = ts; j < ts + 3; ++j) {
					if (board[i][j] == num) {
						board[first][second] = 0;
						return;
					}
				}
				if (isInsert == false)
					break;
			}

			board[first][second] = num;
		
	}
	
	
	//dfs를 호출하여 숫자를 넣어준다 .
	for (int i = 1; i < 10; ++i) {
		dfs(val + 1, i);
	}
	if (val != -1) {
		int first = gblank[val].first;
		int second = gblank[val].second;
		board[first][second] = 0;
	}

	return;
}

int main(void) {

	for (int i = 0; i < SDQ; ++i) {
		for (int j = 0; j < SDQ; ++j) {
			cin >> board[i][j];
			if (board[i][j] == 0) {
				cnt++; // 0 인칸만큼 늘려준다.
				gblank.emplace_back(i, j);
			}
		}
	}

	dfs(-1 , 0);
}