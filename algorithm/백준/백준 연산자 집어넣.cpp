#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
const int MAXVALUE = 1000000000 + 1;
constexpr int MAX = 11;
int number[MAX + 1];
int op[4];
int n;

int maxval = -MAXVALUE;
int minval = MAXVALUE;
void dfs(int cnt , int plus , int minus , int multply, int divide , int sum) {

	if (cnt == n) { // n -1 개를 조합하면 결과값을 저장하면 되므로 재귀 도는것을 생각해 n 까지 돌면 저장 
		maxval = max(maxval, sum);
		minval = min(minval, sum);
		return;
	}

	if (plus > 0)
		dfs(cnt + 1, plus - 1, minus, multply, divide, sum + number[cnt]);
	if (minus > 0)
		dfs(cnt + 1, plus, minus - 1, multply, divide, sum - number[cnt]);
	if (multply > 0)
		dfs(cnt + 1, plus, minus, multply - 1, divide, sum * number[cnt]);
	if (divide > 0)
		dfs(cnt + 1, plus, minus, multply, divide - 1, sum / number[cnt]);
	return;
}


int main(void) {
	cin >> n;
	for (int i = 0; i < n; ++i) {
		cin >> number[i];
	}
	for (int i = 0; i < 4; ++i) {
		cin >> op[i];
	}
	dfs(1 , op[0] , op[1] , op[2] ,op[3] , number[0]);

	cout << maxval << "\n";
	cout << minval << "\n";
	return 0;

}